Docker Environment for Phalcon
==============================

Containers for Phalcon project development environment.

### How To Use

To make your setup process more efficient open up a second tab in your Shell client and download all needed docker images with the following command. It will happen automatically during the next steps, but since it might take a while depending on your connection it would be wise to run it separately

        $ docker pull redis:alpine \
        && docker pull percona:5.7.19 \
        && docker pull medeirosleandro/tls:latest \
        && docker pull medeirosleandro/varnish:latest \
        && docker pull library/nginx:alpine \
        && docker pull medeirosleandro/php-phalcon:7.2-fpm \
        && docker pull medeirosleandro/php-phalcon:7.2-cli;

1. Copy the custom local hosts of the project to your hosts file

        $ cat ./home/localhosts | sudo tee -a /etc/hosts

2. \[MAC USERS ONLY] Setup your NFS to be used with docker:

    Catalina or newer:

        $ tools/setup_native_nfs_docker_osx.sh

    Older OSX versions:

        $ tools/setup_native_nfs_docker_osx_legacy.sh

3. Start the containers and leave PHP CLI to be built separately

        $ docker-compose up -d php-cli --scale php-cli=0

4. Install the dependencies through CLI

        $ docker-compose run --rm php-cli zsh
        
        $ composer install -vvv

5. Access the test environment using the following URLs:

    * Developer mode: [http://localhost] or [https://localhost]

    * Production mode: [http://coolblue.local] or [https://coolblue.local]


### Host Requirements

- Docker
- Docker Compose
- \[Optional] NFS support. In case your OS does not support NFS there's an alternative docker setup in the file `docker-compose.no-nfs.yml`

> **MySQL Container**

- Command `db`: Log's in to MySQL selecting the _coolblue_ database
- Command `dbroot`: Log's in to MySQL selecting the _coolblue_ database with adminstrative privileges
- Command `dbdump`: Runs to MySQL Dump utility selecting the _coolblue_ database
