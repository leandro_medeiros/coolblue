<?php

namespace Coolblue\Library\Auth;

/**
 * Class Exception
 *
 * @category    coolblue
 * @package     Coolblue\Library\Auth
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 */
class Exception extends \Coolblue\Library\Util\Exception
{
    const USR_UNKNOWN = 101;
    const PWD_WRONG = 102;
    const USR_INATIVE = 103;
    const USR_BANNED = 104;
    const USR_SUSPENDED = 105;

    private static $messages = [
        self::USR_UNKNOWN => 'Unknown user',
        self::PWD_WRONG => 'Incorrect password',
        self::USR_INATIVE => 'This user has been deactivated',
        self::USR_BANNED => 'This user has been banned',
        self::USR_SUSPENDED => 'This user has been suspended',
    ];

    /**
     * Exception constructor.
     *
     * @param int $code
     */
    public function __construct(int $code)
    {
        return parent::__construct(self::$messages[$code], $code);
    }
}
