<?php

namespace Coolblue\Library\Auth;

use Coolblue\Library\Auth\Exception as AuthEx;
use Coolblue\Model\FailedLogin;
use Coolblue\Model\RememberToken;
use Coolblue\Model\Repository\User as UserRepository;
use Coolblue\Model\User;
use DateTime;
use Phalcon\Mvc\User\Component;
use function date;

/**
 * Class Auth
 *
 * @category    coolblue
 * @package     Coolblue\Library\Auth
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 */
class Auth extends Component
{
    /**
     * @var \Coolblue\Model\Repository\User
     */
    protected $userRepository;

    /**
     * Auth constructor.
     */
    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }

    /**
     * @param array $credentials
     *
     * @return bool
     * @throws \Coolblue\Library\Auth\Exception
     * @throws \Coolblue\Library\Util\Exception
     */
    public function check(array $credentials)
    {
        /** @var \Coolblue\Model\User $user */
        $user = $this->userRepository->getByEmail($credentials['email']);

        if ($user == false || !$user->getId()) {
            $this->registerUserThrottling();
            throw new AuthEx(AuthEx::USR_UNKNOWN);
        }

        if (!$this->security->checkHash($credentials['password'], $user->getPassword())) {
            $this->registerUserThrottling($user->getId());
            throw new AuthEx(AuthEx::PWD_WRONG);
        }

        $this->checkUserFlags($user);

        $this->userRepository->saveSuccessLogin($user, $this->request);

        if (isset($credentials['remember'])) {
            $token = $this->userRepository->createRememberEnvironment(
                $user,
                $this->request->getUserAgent()
            );

            if ($token) {
                $expire = time() + 86400 * 8;
                $this->cookies->set('RMU', $user->getId(), $expire);
                $this->cookies->set('RMT', $token, $expire);
            }
        }

        $this->session->set('auth-identity', [
            'id' => $user->getId(),
            'name' => $user->getName(),
            'profile' => $user->getResource()->profile->name,
            'session_start_time' => date(
                $this->di->get('config')->application->short_date_format
            )
        ]);

        return true;
    }

    /**
     * @param int|null $userId
     */
    public function registerUserThrottling(int $userId = null)
    {
        $failedLogin = new FailedLogin();
        $failedLogin->setUserId($userId);
        $failedLogin->setIpAddress($this->request->getClientAddress());

        if (!$failedLogin->save()) {
            $this->di->get('logger')->error(
                print_r($failedLogin->getMessages(), true)
            );
        }

        $attempts = FailedLogin::count([
            'ipAddress = ?0 AND attemptedAt >= ?1',
            'bind' => [
                $this->request->getClientAddress(),
                time() - 3600 * 6,
            ],
        ]);

        switch ($attempts) {
            case 0:
            case 1:
            case 2:
                # No delay
                break;
            case 3:
            case 4:
                sleep(2);
                break;
            default:
                sleep(5);
                break;
        }
    }

    /**
     * @return bool
     */
    public function hasRememberMe()
    {
        return $this->cookies->has('RMU');
    }

    /**
     * @return bool
     * @throws \Coolblue\Library\Auth\Exception
     * @throws \Coolblue\Library\Util\Exception
     */
    public function loginWithRememberMe()
    {
        $me = $this;
        $userId = $this->cookies->get('RMU')->getValue();
        $cookieToken = $this->cookies->get('RMT')->getValue();

        /** @var \Coolblue\Model\User $user */
        $user = User::findFirstById($userId);

        $fncFail = function () use ($me) {
            $me->cookies->get('RMU')->delete();
            $me->cookies->get('RMT')->delete();
            return false;
        };

        if (!$user) {
            return $fncFail();
        }

        $userAgent = $this->request->getUserAgent();
        $token = md5($user->getEmail() . $user->getPassword() . $userAgent);

        if ($cookieToken != $token) {
            return $fncFail();
        }

        /** @var \Coolblue\Model\RememberToken $remember */
        $remember = RememberToken::findFirst([
            'userId = ?0 AND token = ?1',
            'bind' => [
                $user->getId(),
                $token,
            ],
        ]);

        $maxDuration = $this->di->get('config')->application->auth_duration;
        $now = new DateTime();

        if (!$remember || $remember->getCreatedAt()->diff($now)->days > $maxDuration) {
            return $fncFail();
        }

        $this->checkUserFlags($user);

        $this->session->set('auth-identity', [
            'id' => $user->getId(),
            'name' => $user->getName(),
            'profile' => $user->profile->name,
            'session_start_time' => $remember->getCreatedAt()->format(
                $this->di->get('config')->application->short_date_format
            )
        ]);

        $this->userRepository->saveSuccessLogin($user, $this->request);

        return true;
    }

    /**
     * @param \Coolblue\Model\User $user
     *
     * @throws \Coolblue\Library\Auth\Exception
     */
    public function checkUserFlags(User $user)
    {
        if (!$user->getActive()) {
            throw new AuthEx(AuthEx::USR_INATIVE);
        }

        if ($user->getBanned()) {
            throw new AuthEx(AuthEx::USR_BANNED);
        }

        if ($user->getSuspended()) {
            throw new AuthEx(AuthEx::USR_SUSPENDED);
        }
    }

    /**
     * @return mixed
     */
    public function getIdentity()
    {
        return $this->session->get('auth-identity');
    }

    /**
     * @return mixed
     */
    public function getIdentityName()
    {
        $identity = $this->getIdentity();
        return $identity['name'];
    }

    /**
     * @return mixed
     */
    public function getIdentityId()
    {
        $identity = $this->getIdentity();
        return $identity['id'];
    }

    /**
     * @return mixed
     */
    public function getIdentitySessionStart()
    {
        $identity = $this->getIdentity();
        return $identity['session_start_time'];
    }

    /**
     * @return mixed
     */
    public function getIdentityProfile()
    {
        $identity = $this->getIdentity();
        return $identity['profile'];
    }

    /**
     * Removes user auth info
     */
    public function remove()
    {
        if ($this->cookies->has('RMU')) {
            $this->cookies->get('RMU')->delete();
        }

        if ($this->cookies->has('RMT')) {
            $this->cookies->get('RMT')->delete();
        }

        $this->session->remove('auth-identity');
    }

    /**
     * @param int $id
     *
     * @throws \Coolblue\Library\Auth\Exception
     */
    public function authUserById(int $id)
    {
        /** @var \Coolblue\Model\User $user */
        $user = $this->userRepository->getById($id);

        if ($user == false) {
            throw new AuthEx(AuthEx::USR_UNKNOWN);
        }

        $this->checkUserFlags($user);
        $this->session->set('auth-identity', [
            'id' => $user->getId(),
            'name' => $user->getName(),
            'profile' => $user->profile->name,
        ]);
    }

    /**
     * @return bool|User
     * @throws \Coolblue\Library\Auth\Exception
     */
    public function getUser()
    {
        $identity = $this->session->get('auth-identity');

        if (!isset($identity['id'])) {
            return false;
        }

        $user = User::findFirstById($identity['id']);

        if ($user == false) {
            throw new AuthEx(AuthEx::USR_UNKNOWN);
        }

        return $user;
    }
}
