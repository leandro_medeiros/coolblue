<?php

namespace Coolblue\Library\Acl;

use Coolblue\Model\Profile;
use DirectoryIterator;
use Phalcon\Acl\Adapter\Memory as AclMemory;
use Phalcon\Acl\Resource as AclResource;
use Phalcon\Acl\Role as AclRole;
use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\User\Component;
use RegexIterator;

/**
 * Class Acl
 *
 * @category    coolblue
 * @package     Coolblue\Library\Acl
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 */
class Acl extends Component
{
    /**
     * @var string
     */
    const CONTROLLER_NAMESPACE = '\Coolblue\Controller\\';

    /**
     * @var string
     */
    const SESSION_KEY = 'resources';

    /**
     * @var string
     */
    const STORAGE_KEY = 'coolblue-acl';

    /**
     * @var int
     */
    const RESOURCE_PRIVATE = 1;

    /**
     * @var int
     */
    const RESOURCE_PUBLIC = 0;

    /**
     * @var \Phalcon\Acl\Adapter\Memory
     */
    private $acl;

    /**
     * @var \Phalcon\DI\FactoryDefault
     */
    protected $di;

    /**
     * @var string
     */
    protected $cacheDir;

    /**
     * @var string
     */
    private $filePath = 'acl/data.txt';

    /**
     * @var array
     */
    private $resources = [false => [], true => []];

    /**
     * @var array
     */
    private $actionDescriptions = [
        'index' => 'Home',
        'search' => 'Search',
        'create' => 'New',
        'edit' => 'Edit',
        'delete' => 'Remove',
        'list' => 'List',
        'listAll' => 'List All',
        'getInfo' => 'Get Info'
    ];

    /**
     * Acl constructor.
     *
     * @param \Phalcon\DI\FactoryDefault $di
     */
    public function __construct(FactoryDefault $di)
    {
        $this->di = $di;
        $this->cacheDir = $this->di->get('config')->realpath->cache;
        $session = $this->di->get('session');

        if ($session->has(self::SESSION_KEY)) {
            $this->resources = unserialize($session->get(self::SESSION_KEY));
        } else {
            $session->set(self::SESSION_KEY, serialize($this->refreshResources()));
        }
    }

    /**
     * @param string $controllerName
     *
     * @return bool
     */
    public function isPrivate(string $controllerName)
    {
        return isset($this->resources[true][strtolower($controllerName)]);
    }

    /**
     * @param string $profile
     * @param string $controller
     * @param string $action
     *
     * @return bool
     */
    public function isAllowed(string $profile, string $controller, string $action)
    {
        return $this->getAcl()->isAllowed($profile, $controller, $action);
    }

    /**
     * @return mixed|\Phalcon\Acl\Adapter\Memory
     */
    public function getAcl()
    {
        if (is_object($this->acl)) {
            return $this->acl;
        }

        if (function_exists('apc_fetch')) {
            $acl = apc_fetch(self::STORAGE_KEY);
            if (is_object($acl)) {
                $this->acl = $acl;
                return $acl;
            }
        }

        if (!file_exists($this->cacheDir . $this->filePath)) {
            $this->acl = $this->rebuild();
            return $this->acl;
        }

        $data = file_get_contents($this->cacheDir . $this->filePath);
        $this->acl = unserialize($data);

        if (function_exists('apc_store')) {
            apc_store(self::STORAGE_KEY, $this->acl);
        }

        return $this->acl;
    }

    /**
     * @param \Coolblue\Model\Profile $profile
     *
     * @return array
     */
    public function getPermissions(Profile $profile)
    {
        $permissions = [];
        foreach ($profile->getPermissions() as $permission) {
            $permissions[$permission->resource . '.' . $permission->action] = true;
        }
        return $permissions;
    }

    /**
     * Returns all the resources and their actions available in the application
     *
     * @return array
     */
    public function refreshResources()
    {
        $dir = new DirectoryIterator($this->di->get('config')->realpath->app . 'controller');
        $files = new RegexIterator($dir, '@\w+Controller.php$@');
        $resources = [];

        foreach ($files as $item) {
            if ($item->isFile()) {
                $class = strtolower($item->getBasename('Controller.php'));
                $qualifiedName = self::CONTROLLER_NAMESPACE . $item->getBasename('.php');
                $visibility = $qualifiedName::$isPrivate;

                if (!in_array($class, array_keys($resources[$visibility])) && class_exists($qualifiedName)) {
                    $methods = array_map(function ($value) {
                        return substr($value, 0, -6); # Action (6 chars)
                    }, preg_grep('@\w+Action$@', get_class_methods($qualifiedName)));

                    $resources[$visibility][$class] = $methods;
                }
            }
        }

        return $resources;
    }

    /**
     * @param int|null $visibility
     *
     * @return array|mixed
     */
    public function getResources(int $visibility = null)
    {
        if (empty($this->resources)) {
            $this->resources = $this->refreshResources();
        }

        return empty($visibility) ? $this->resources : $this->resources[$visibility];
    }

    /**
     * @param string $controllerName
     *
     * @return array|mixed
     */
    public function getPublicResources(string $controllerName = '')
    {
        return $this->getResources(self::RESOURCE_PUBLIC);
    }

    /**
     * @return array|mixed
     */
    public function getPrivateResources()
    {
        return $this->getResources(self::RESOURCE_PRIVATE);
    }

    /**
     * @param string $profile
     * @param string $controller
     *
     * @return array|mixed
     */
    public function getAllAllowed(string $profile, string $controller)
    {
        $result = [];
        $availableResources = $this->getPrivateResources();

        if (array_key_exists($controller, $availableResources)) {
            foreach ($availableResources[$controller] as $action) {
                if ($this->isAllowed($profile, $controller, $action)) {
                    $result[] = $action;
                }
            }
        }

        return $result;
    }

    /**
     * @param string $action
     *
     * @return mixed|string
     */
    public function getActionDescription(string $action)
    {
        if (isset($this->actionDescriptions[$action])) {
            return $this->actionDescriptions[$action];
        } else {
            return $action;
        }
    }

    /**
     * @return \Phalcon\Acl\Adapter\Memory
     */
    public function rebuild()
    {
        $acl = new AclMemory();

        $acl->setDefaultAction(\Phalcon\Acl::DENY);

        $profiles = Profile::find('active = true');

        foreach ($profiles as $profile) {
            $acl->addRole(new AclRole($profile->name));
        }

        foreach ($this->resources[true] as $resource => $actions) {
            $acl->addResource(new AclResource($resource), $actions);
        }

        foreach ($profiles as $profile) {
            foreach ($profile->getPermissions() as $permission) {
                $acl->allow($profile->name, $permission->resource, $permission->action);
            }
        }

        if (touch($this->cacheDir . $this->filePath) && is_writable($this->cacheDir . $this->filePath)) {
            file_put_contents($this->cacheDir . $this->filePath, serialize($acl));

            if (function_exists('apc_store')) {
                apc_store(self::STORAGE_KEY, $acl);
            }
        } else {
            $this->flash->error(
                'User running current script does not have write rights on ' . $this->cacheDir . $this->filePath
            );
        }

        return $acl;
    }
}
