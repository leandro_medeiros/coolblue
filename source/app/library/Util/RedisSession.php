<?php

declare(strict_types = 1);

namespace Coolblue\Library\Util;

use Phalcon\Session\Adapter\Redis;

/**
 * Class RedisSession
 *
 * @category    coolblue
 * @package     Coolblue\Library\Util
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 */
class RedisSession
{
    /** @var Redis */
    private static $instance;

    /**
     * @param array $options
     *
     * @return \Phalcon\Session\Adapter\Redis
     */
    public static function getInstance(array $options)
    {
        if (!isset(self::$instance)) {
            self::$instance = new Redis($options);
        }

        return self::$instance;
    }
}
