<?php

declare(strict_types = 1);

namespace Coolblue\Library\Util;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\TestResult;
use PHPUnit\Framework\TestSuite;
use PHPUnit\Util\Log\JUnit;
use ReflectionClass;
use SimpleXMLElement;

/**
 * Class AbstractTestCase
 *
 * @category    coolblue
 * @package     Coolblue\Library\Util
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 */
abstract class AbstractTestCase extends TestCase
{
    /**
     * @var string
     */
    public static $title = 'Unit Test';

    /**
     * @var string
     */
    public static $description = 'PHPUnit test case for Coolblue';

    /**
     * Controller subject test
     * @var string
     */
    protected static $subjectClass;

    /**
     * Controller instance
     * @var Object
     */
    protected static $subjectObject;

    /**
     * AbstractTestCase constructor.
     *
     * @param null   $name
     * @param array  $data
     * @param string $dataName
     */
    public function __construct($name = NULL, array $data = [], $dataName = '')
    {
        $this->backupGlobals = false;
        parent::__construct($name, $data, $dataName);
    }

    /**
     * @param      $id
     * @param      $title
     * @param null $extraData
     *
     * @throws \Coolblue\Library\Util\Exception
     */
    protected static function log($id, $title, $extraData = null) {
        $log = str_repeat('-', 100) . PHP_EOL
            . '#%s# -> %s%s' . PHP_EOL
            . str_repeat('-', 100) . PHP_EOL . PHP_EOL;

        $extraData = empty($extraData) ? '' : ': ' . print_r($extraData, true);
        $id        = strtoupper($id);

        file_put_contents(
            BASE_DIR . '/var/logs/' . static::$subjectClass . 'Test.log',
            sprintf($log, $id, $title, $extraData),
            FILE_APPEND
        );

        if ($id == 'FAIL') {
            throw new Exception($title);
        }
    }

    /**
     * @throws \Coolblue\Library\Util\Exception
     */
    protected function setUp(): void {
        $debug = debug_backtrace();
        self::log('teststart', $debug[0]['object']->getName());
    }

    /**
     * @throws \Coolblue\Library\Util\Exception
     */
    protected function tearDown(): void {
        $debug = debug_backtrace();
        self::log('testend', $debug[0]['object']->getName());
    }

    /**
     * @param string $message
     *
     * @throws \Coolblue\Library\Util\Exception
     */
    public static function markTestSkipped(string $message = ''): void {
        self::log('skipped', $message);
        parent::markTestSkipped($message);
    }

    /**
     * @param $subjectClass
     *
     * @throws \ReflectionException
     */
    protected static function setUpService($subjectClass) {
        self::$subjectClass = $subjectClass;

        if (!class_exists(self::$subjectClass)) {
            die('Class "' . self::$subjectClass . '" not found');
        }

        $ref = new ReflectionClass(self::$subjectClass);
        self::$subjectObject = $ref->newInstance();
    }

    /**
     * @param string $className
     *
     * @return false|string
     */
    public static function execute(string $className)
    {
        $fullyQualifiedName = sprintf('\Coolblue\Test\%s', $className);

        if (!class_exists($fullyQualifiedName)) {
            return '<h1 class="text-center flex-fill">Test not found</h1>';
        }

        $suite = new TestSuite($fullyQualifiedName);
        $listener = new JUnit();
        $testResult = new TestResult();
        $testResult->addListener($listener);
        $suite->run($testResult);

        $result = new SimpleXMLElement($listener->getXML());

        ob_start();
        include BASE_DIR . '/public/unittests/simple.tpl.php';
        $html = ob_get_contents();
        ob_end_clean();

        return $html;
    }
}

