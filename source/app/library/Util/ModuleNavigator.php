<?php

declare(strict_types = 1);

namespace Coolblue\Library\Util;

use DirectoryIterator;
use Phalcon\Config;
use Phalcon\DiInterface;
use Phalcon\Session\AdapterInterface;
use RegexIterator;

/**
 * Class ModuleNavigator
 *
 * @category    coolblue
 * @package     Coolblue\Library\Util
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 */
class ModuleNavigator
{
    /**
     * @var string
     */
    const CONTROLLER_NAMESPACE = '\Coolblue\Controller\\';

    /**
     * @var string
     */
    const SESSION_KEY = 'modules';

    /**
     * @param \Phalcon\DiInterface $di
     * @param bool                 $forceRefresh
     *
     * @return array|mixed
     */
    public static function getModules(DiInterface $di, bool $forceRefresh = false)
    {
        /** @var AdapterInterface $session */
        $session = $di->get('session');

        if (!$forceRefresh && $session->has(self::SESSION_KEY)) {
            return unserialize($session->get(self::SESSION_KEY));
        } else {
            $modules = self::refreshModules($di->get('config'));
            $session->set(self::SESSION_KEY, serialize($modules));
            return $modules;
        }
    }

    /**
     * Returns all the resources and their actions available in the application
     *
     * @param \Phalcon\Config $config
     *
     * @return array
     */
    protected static function refreshModules(Config $config)
    {
        $dir = new DirectoryIterator($config->realpath->app . 'controller');
        $files = new RegexIterator($dir, '@\w+Controller.php$@');
        $modules = [];

        foreach ($files as $item) {
            if ($item->isFile()) {
                $class = strtolower($item->getBasename('Controller.php'));
                $qualifiedName = self::CONTROLLER_NAMESPACE . $item->getBasename('.php');

                if ($qualifiedName::$showInNavigator) {
                    $modules[$class] = [
                        'private' => $qualifiedName::$isPrivate,
                        'title' => $qualifiedName::$title,
                        'order' => $qualifiedName::$sortOrder,
                        'target' => $class
                    ];
                }
            }
        }

        return self::sortModules($modules);
    }

    /**
     * @param array $modules
     *
     * @return array
     */
    protected static function sortModules(array $modules)
    {
        uasort($modules, function($a, $b) {
            return $a['order'] <=> $b['order'];
        });

        return $modules;
    }
}
