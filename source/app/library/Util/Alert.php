<?php

namespace Coolblue\Library\Util;

/**
 * Class Alert
 *
 * @category    coolblue
 * @package     Coolblue\Library\Util
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 */
class Alert
{
    /**
     * Alert types
     */
    const ERROR = "-danger";
    const WARNING = "-warning";
    const SUCCESS = "-success";
    const OTHER = "-info";
    const SESSION_INDEX = 'alert_idx';

    /**
     * Internal messages
     */
    const MSG_SYSTEM_ERROR = '';
    const MSG_SYSTEM_ATTEMPTS = '';
    const MSG_SYSTEM_LOGIN_ERROR = 'Sessão inválida/expirada';
    const MSG_SYSTEM_EXPIRED_SESSION = 'Sua sessão expirou, faça login novamente';
    const MSG_SYSTEM_CONNECTION_OUT = 'Erro de conexão ao servidor. Tente novamente dentro de instantes';
    const MSG_SYSTEM_METHOD_FAIL = 'Falha ao processar requisição: O método <strong>%s->%s</strong> não existe.';

    const MSG_DB_CONNECTION_FAIL = 'Falha de conexão ao banco de dados: "%s".';

    /**
     * Login messages
     */
    const MSG_LOGIN_UNKNOWN_ERROR = 'Ocorreu um erro e não foi possivel prosseguir';
    const MSG_LOGIN_INVALID_USER = 'Tipo de usuário inválido.';
    const MSG_LOGIN_INACTIVE = 'Seu usuário foi desativado. Entre em contato com o administrador do sistema';
    const MSG_LOGIN_WRONG_PASS = 'Usuário/senha incorreto';
    const MSG_LOGIN_TIMEOUT = 'Tempo limite excedido, tente novamente em instantes';

    /**
     * User messages
     */
    const MSG_USER_EMPTY = 'Não há usuários cadastrados';
    const MSG_USER_FIELDS = 'Informe os campos corretamente';
    const MSG_USER_GET_INFO_FAIL = 'Falha ao obter dados do usuário autenticado. Por favor tente novamente.';
    const MSG_USER_INVALID_TYPE = 'Tipo de usuário inválido.';

    /**
     * Module messages
     */
    const MSG_MODULE_ACCESS_DENIED = 'Você não possui os privilégios necessários para acessar este módulo';

    /**
     * General messages
     */
    const MSG_UNKNONW_ERROR = 'Ocorreu um erro ao processar sua requisição, por favor tente novamente mais tarde.';
    const MSG_INVALID_PARAMS = 'Parâmetros inválidos';
    const MSG_PUT_OK = 'Sucesso ao guardar dados!';
    const MSG_PUT_FAIL = 'Falha ao guardar dados: ';
    const MSG_UPDATE_OK = 'Sucesso ao atualizar dados!';
    const MSG_UPDATE_FAIL = 'Falha ao atualizar informações: ';
    const MSG_DELETE_OK = 'Dados apagados com sucesso';
    const MSG_DELETE_FAIL = 'Falha ao apagar dados: ';
    const MSG_NO_RECORDS = 'Não há registros para exibir';

    /**
     * @property integer
     */
    public $id;

    /**
     * @property string
     */
    public $text;

    /**
     * @property string
     */
    public $type;

    /**
     * @property boolean
     */
    public $dismissible;

    public function __construct($text, $type = self::OTHER, $sessionStore = true, $id = '', $dismissible = true)
    {
        $this->text = $text;
        $this->type = $type;
        $this->dismissible = $dismissible;
        $this->id = $id;

        if ($sessionStore) {
            $_SESSION[self::SESSION_INDEX][] = serialize($this);
        }

    }

    private function __sleep()
    {
        return ['text', 'type', 'id', 'dismissible'];
    }

    public function get()
    {
        return '<div id="' . $this->id
        . '" style="padding:10px 35px 10px 10px" class="alert text-left fade in alert'
        . $this->type
        . ($this->dismissible ? ' alert-dismissible' : '')
        . '" role="alert" >'
        . $this->text
            . (!$this->dismissible ? '' : '<button type="button" class="close" data-dismiss="alert" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>')
            . '</div>';
    }

    public function show()
    {
        echo $this->get();
    }

    public static function showAlert($text, $type = self::OTHER, $id = '', $dismissible = true)
    {
        echo '<div id="' . $id
            . '" style="padding:10px 35px 10px 10px" class="alert text-left alert'
            . $type
            . ($dismissible ? ' alert-dismissible' : '')
            . '" role="alert" >'
            . $text
            . (!$dismissible ? '' : '<button type="button" class="close" data-dismiss="alert" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>')
            . '</div>';
    }

    public static function showAll($unset = true)
    {
        if (!empty($_SESSION[self::SESSION_INDEX])) {

            foreach ($_SESSION[self::SESSION_INDEX] as $Alert) {
                unserialize($Alert)->show();
            }
        }

        if ($unset) {
            unset($_SESSION[self::SESSION_INDEX]);
        }
    }
}
