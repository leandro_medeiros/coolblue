<?php

namespace Coolblue\Library\Util;

use Throwable;

/**
 * Class Exception
 *
 * @package Coolblue\Library\Util
 */
class Exception extends \Exception
{
    const ERR_MODEL_SAVE_FAIL = 66600;
    const ERR_REQ_FIND_DEVICE_NOT_FOUND = 66601;

    /**
     * @var array
     */
    protected static $errors = [
        self::ERR_REQ_FIND_DEVICE_NOT_FOUND => 'Device with MAC Address "%s" not found'
    ];

    /**
     * @param int             $code
     * @param array           $msgValues
     * @param \Throwable|null $throwable
     *
     * @return \Coolblue\Library\Util\Exception
     */
    public static function create(int $code, array $msgValues = [], Throwable $throwable = null) {
        return new self(
            vsprintf(self::$errors[$code], $msgValues),
            $code,
            $throwable
        );
    }
}
