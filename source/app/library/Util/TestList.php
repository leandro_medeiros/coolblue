<?php

declare(strict_types = 1);

namespace Coolblue\Library\Util;

use Phalcon\DiInterface;
use RegexIterator;

/**
 * Class TestList
 *
 * @category    coolblue
 * @package     Coolblue\Library\Util
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 */
class TestList extends \DirectoryIterator
{
    /**
     * @var \Phalcon\DiInterface
     */
    protected $di;

    /**
     * TestList constructor.
     *
     * @param \Phalcon\DiInterface $di
     */
    public function __construct(DiInterface $di)
    {
        $this->di = $di;

        parent::__construct($this->di->get('config')->realpath->tests);
    }

    /**
     * @return array
     */
    public function getTestsInfo()
    {
        $testConfig = $this->di->get('config')->tests;
        $files = new RegexIterator($this, $testConfig->regex);
        $result = [];

        foreach ($files as $test) {
            if (!$test->isFile()) {
                continue;
            }

            $class = $test->getBasename($testConfig->fileSuffix);
            $qualifiedName = $testConfig->namespace . $class;

            if (!in_array($class, array_keys($result)) && class_exists($qualifiedName)) {
                $methods = array_map(function ($value) {
                    return substr($value, 0, -6); # Action (6 chars)
                }, preg_grep($testConfig->methodRegex, get_class_methods($qualifiedName)));

                $result[$class] = [
                    'description' => $qualifiedName::$description,
                    'title' => $qualifiedName::$title,
                    'tests' => $methods,
                    'testCount' => count($methods),
                ];
            }
        }

        return $result;
    }
}
