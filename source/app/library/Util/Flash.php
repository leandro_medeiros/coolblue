<?php

namespace Coolblue\Library\Util;

use Phalcon\Flash\Direct;

/**
 * Messages
 *
 * @author    Leandro M. Medeiros <leandro.m.medeiros@me.com>
 * @since     1.0.0
 * @link      http://github.com/leandrommedeiros/monsterfymvc
 */
class Flash extends Direct
{
    public function message($type, $message)
    {
        $this->_messages[] = [
            'type' => $type,
            'text' => $message
        ];
        parent::message($type, $message);
    }

    /**
     * Saída de todas as mensagens
     *
     * @param null $remove
     *
     * @return string     Mensagens formatadas (HTML)
     * @author Leandro M. Medeiros <leandro.m.medeiros@me.com>
     * @since  2017-05-11
     *
     */
    public function output($remove = null)
    {
        $output = [];

        if (!empty($this->_messages)) {
            foreach ($this->_messages as $message) {
                $cssClass = 'flash-message';
                if (!empty($this->_cssClasses[$message['type']])) {
                    $cssClass = $this->_cssClasses[$message['type']];
                }

                $output[] = sprintf(
                    '<div class="%s">%s<button type="button" class="close" data-dismiss="alert" aria-label="Fechar"><span aria-hidden="true">&times;</span></button></div>',
                    $cssClass,
                    $message['text']
                );
            }
        }

        return implode(PHP_EOL, $output);
    }

    /**
     * Echo da classe
     *
     * @author Leandro M. Medeiros <leandro.m.medeiros@me.com>
     * @since 2017-05-11
     *
     * @return string     Mensagens formatadas (HTML)
     */
    public function __toString()
    {
        return $this->output();
    }
}
