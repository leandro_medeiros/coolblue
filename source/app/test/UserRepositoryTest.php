<?php

declare(strict_types = 1);

namespace Coolblue\Test;

use Coolblue\Library\Util\AbstractTestCase;
use Coolblue\Model\Repository\User as UserRepository;
use Coolblue\Model\User as UserModel;

/**
 * Class UserRepositoryTest
 *
 * @category    coolblue
 * @package     Coolblue\Test
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 */
class UserRepositoryTest extends AbstractTestCase
{
    /**
     * @var string
     */
    public static $title = 'User Unit Tests';

    /**
     * @var string
     */
    public static $description = 'PHPUnit test case for the User Model Repository class.';

    /**
     * @throws \ReflectionException
     */
    public static function setUpBeforeClass(): void
    {
        self::setUpService(UserRepository::class);
    }

    /**
     * Tests \Coolblue\Model\Repository\User::getByEmail()
     */
    public function testGetByEmail()
    {
        $empty = static::$subjectObject::getByEmail('random@email.com');
        $notEmpty = static::$subjectObject::getByEmail('admin@coolblue.nl');

        $this->assertInstanceOf(UserModel::class, $empty);
        $this->assertInstanceOf(UserModel::class, $notEmpty);

        $this->assertEmpty($empty->getId());

        $notEmpty = $notEmpty->toArray();
        $this->assertArrayHasKey('id', $notEmpty);
        $this->assertArrayHasKey('name', $notEmpty);
        $this->assertArrayHasKey('email', $notEmpty);
        $this->assertArrayHasKey('password', $notEmpty);
        $this->assertArrayHasKey('mustChangePassword', $notEmpty);
        $this->assertArrayHasKey('profileId', $notEmpty);
        $this->assertArrayHasKey('banned', $notEmpty);
        $this->assertArrayHasKey('suspended', $notEmpty);
        $this->assertArrayHasKey('active', $notEmpty);
    }

    /**
     * Tests \Coolblue\Model\Repository\User::getById()
     */
    public function testGetById()
    {
        $empty = static::$subjectClass::getById(8);
        $notEmpty = static::$subjectClass::getById(1);

        $this->assertInstanceOf(UserModel::class, $empty);
        $this->assertInstanceOf(UserModel::class, $notEmpty);

        $this->assertEmpty($empty->getId());

        $notEmpty = $notEmpty->toArray();
        $this->assertArrayHasKey('id', $notEmpty);
        $this->assertArrayHasKey('name', $notEmpty);
        $this->assertArrayHasKey('email', $notEmpty);
        $this->assertArrayHasKey('password', $notEmpty);
        $this->assertArrayHasKey('mustChangePassword', $notEmpty);
        $this->assertArrayHasKey('profileId', $notEmpty);
        $this->assertArrayHasKey('banned', $notEmpty);
        $this->assertArrayHasKey('suspended', $notEmpty);
        $this->assertArrayHasKey('active', $notEmpty);
    }
}
