<?php

declare(strict_types = 1);

namespace Coolblue\Test;

use Coolblue\Library\Util\AbstractTestCase;
use Coolblue\Model\Repository\Cart as CartRepository;
use Phalcon\Mvc\Model\ResultsetInterface;

/**
 * Class CartRepositoryTest
 *
 * @category    coolblue
 * @package     Coolblue\Test
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 */
class CartRepositoryTest extends AbstractTestCase
{
    /**
     * @var string
     */
    public static $title = 'Cart Unit Tests';

    /**
     * @var string
     */
    public static $description = 'PHPUnit test case for the Shopping Cart Model Repository class of the Coolblue interview test.';

    /**
     * @throws \ReflectionException
     */
    public static function setUpBeforeClass(): void
    {
        self::setUpService(CartRepository::class);
    }

    /**
     * Tests Coolblue\Model\Repository\Cart::getListByUser()
     */
    public function testGetListByUser()
    {
        $empty = static::$subjectObject::getListByUser(0);
        $notEmpty = static::$subjectObject::getListByUser(1);

        $this->assertInstanceOf(ResultsetInterface::class, $empty);
        $this->assertCount(0, $empty);

        $this->assertInstanceOf(ResultsetInterface::class, $notEmpty);
        $this->assertCount(1, $notEmpty);

        $notEmpty = $notEmpty->toArray();
        $this->assertArrayHasKey('id', $notEmpty[0]);
        $this->assertArrayHasKey('createdAt', $notEmpty[0]);
    }

    /**
     * Tests Coolblue\Model\Repository\Cart::getAllActiveList()
     */
    public function testGetAllActiveList()
    {
        $result = static::$subjectObject::getAllActiveList(1);
        $this->assertInstanceOf(ResultsetInterface::class, $result);
        $this->assertCount(6, $result);

        $result = $result->toArray();
        $this->assertArrayHasKey('id', $result[0]);
        $this->assertArrayHasKey('userId', $result[0]);
        $this->assertArrayHasKey('createdAt', $result[0]);
        $this->assertArrayHasKey('ownedByUser', $result[0]);
        $this->assertArrayHasKey('owner', $result[0]);
    }

    /**
     * Tests Coolblue\Model\Repository\Cart::getCartItems()
     */
    public function testGetItems()
    {
        $empty = static::$subjectObject::getCartItems(4);
        $notEmpty = static::$subjectObject::getCartItems(1);

        $this->assertIsArray($empty);
        $this->assertEmpty($empty);

        $this->assertIsArray($notEmpty);
        $this->assertNotEmpty($notEmpty);

        $notEmpty = reset($notEmpty);
        $this->assertArrayHasKey('itemCreatedAt', $notEmpty);
        $this->assertArrayHasKey('product', $notEmpty);
        $this->assertArrayHasKey('class', $notEmpty);
        $this->assertArrayHasKey('quantity', $notEmpty);
        $this->assertArrayHasKey('unitPrice', $notEmpty);
        $this->assertArrayHasKey('totalPrice', $notEmpty);
    }

    /**
     * Tests Coolblue\Model\Repository\Cart::getInfo()
     */
    public function testGetInfo()
    {
        $empty = static::$subjectObject::getInfo(4);
        $notEmpty = static::$subjectObject::getInfo(1);

        $this->log('array', 'Empty Result', $empty);
        $this->log('array', 'Not Empty Result', $notEmpty);

        $this->assertIsArray($empty);
        $this->assertArrayHasKey('msg', $empty);

        $this->assertIsArray($notEmpty);
        $this->assertArrayHasKey('owner', $notEmpty);
        $this->assertArrayHasKey('createdAt', $notEmpty);
        $this->assertArrayHasKey('totalItems', $notEmpty);
        $this->assertArrayHasKey('totalPrice', $notEmpty);
        $this->assertArrayHasKey('items', $notEmpty);
        $this->assertNotEmpty($notEmpty['items']);
    }
}
