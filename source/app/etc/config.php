<?php

use Phalcon\Config;
use Phalcon\Logger;

/**
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 * @link        https://about.me/leandro.medeiros
 */
$config = new Config([
    'database' => [
        'adapter' => 'mysql',
        'host' => 'db',
        'port' => '3333',
        'username' => 'interview',
        'password' => 'interview',
        'dbname' => 'coolblue',
        'charset' => 'utf8',
    ],
    'session' => [
        'uniqueId'   => 'coolblue-interview',
        'host'       => 'redis',
        'port'       => 6379,
        'persistent' => true,
        'lifetime'   => 8600,
        'prefix'     => 'cbs_',
        'index'      => 1,
    ],
    'metadata' => [
        'host'     => 'redis',
        'port'     => 6379,
        'lifetime' => 172800,
        'index'    => 2,
    ],
    'application' => [
        'title' => 'Coolblue',
        'baseDir' => BASE_DIR,
        'version' => '1.0.0',
        'full_date_format' => 'd/m/Y \a\t H:i:s',
        'short_date_format' => 'd/m/y \a\t H:i',
        'default_charset' => 'utf-8',
        'environment' => ENVIRONMENT,
        'auth_duration' => 8, # days
        'publicUrl' => isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '',
        'cryptSalt' => 'C]78Ur5{V6pE8LUyJ;Hl_5W}02I(?J!9',
    ],
    'tests' => [
        'namespace' => '\Coolblue\Test\\',
        'regex' => '@\w+Test.php$@',
        'fileSuffix' => '.php',
        'methodRegex' => '@test\w+$@'
    ]
]);

$config->merge(new Config([
    'realpath' => [
        'app' => $config->application->baseDir . '/app/',
        'cache' => $config->application->baseDir . '/var/cache/',
        'config' => $config->application->baseDir . '/app/etc/',
        'tests' => $config->application->baseDir . '/app/test/',
        'var' => $config->application->baseDir . '/var/',
        'vendor' => $config->application->baseDir . '/vendor/',
        'view' => $config->application->baseDir . '/app/views/'
    ]
]));

$config->merge(new Config([
    'view' => [
        'baseDir' => $config->realpath->view,
        'layoutsDir' => $config->realpath->view . '/layouts/',
        'componentsDir' => $config->realpath->view . '/components/',
        'templatesDir' => $config->realpath->view . '/templates/',
    ],
    'publicpath' => [
        'css' => '/assets/css/',
        'fonts' => '/assets/fonts/',
        'images' => '/assets/images/',
        'js' => '/assets/js/',
        'docs' => '/documentation/'
    ],
    'logger' => [
        'path' => $config->realpath->var . 'logs/',
        'format' => '%date% [%type%] %message%',
        'date' => 'r',
        'logLevel' => Logger::DEBUG,
        'filename' => 'application.log'
    ]
]));

$config->merge(new Config([
    'globalAssets' => [
        'css' => [
            $config->publicpath->css . 'vendor/bootstrap.min.css',
            $config->publicpath->css . 'vendor/bootstrap-grid.min.css',
            $config->publicpath->css . 'vendor/bootstrap-reboot.min.css',
            $config->publicpath->css . 'app/monsterfy.min.css',
        ],
        'js' => [
            $config->publicpath->js . 'vendor/jquery.min.js'
        ],
        'requireJs' => [
            'path' => $config->publicpath->js . 'vendor/require.js',
            'local' => true,
            'filter' => false,
            'attributes' => [
                'data-main' => $config->publicpath->js . 'main.min.js?v=1.0.0.0'
            ],
        ],
    ]
]));

return $config;
