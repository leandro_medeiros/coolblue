<?php

use Phalcon\Mvc\Router;

$router = new Router();

### Custom routes definition
$router->add('/', [
    'controller' => 'home',
    'action'     => 'welcome'
]);

return $router;
