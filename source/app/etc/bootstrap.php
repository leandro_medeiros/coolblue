<?php

/**
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 * @link        https://about.me/leandro.medeiros
 */

use Phalcon\Mvc\Application;
use Phalcon\Mvc\Dispatcher;

try {
    # Directories
    define('BASE_DIR', realpath('..'));

    # Current environment definition
    if (!empty($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] != 'localhost') {
        define('ENVIRONMENT', 'PROD');
    }

    if (!defined('ENVIRONMENT')) {
        define('ENVIRONMENT', 'DEV');
    }

    # Error level
    if (ENVIRONMENT == 'DEV') {
        ini_set('display_errors', 'On');
        error_reporting(E_ALL & ~E_DEPRECATED);
    } else {
        error_reporting(E_ERROR);
    }

    # Configuration
    $config = include BASE_DIR . '/app/etc/config.php';

    # Charset
    ini_set('default_charset', $config->application->default_charset);

    # Auto loader
    include $config->realpath->config . 'loader.php';

    # Services
    include $config->realpath->config . 'services.php';

    # Request management
    $application = new Application($di);
    echo $application->handle()->getContent();
} catch (\Phalcon\Mvc\Dispatcher\Exception $ex) {
    if ($di && $di->has('logger')) {
        $di->get('logger')->error(print_r($ex, true));
    }

    switch ($ex->getCode()) {
        case Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
        case Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
            header('location:/error/notfound');
            break;
        default:
            echo '<pre>', $ex->getMessage(), '<br>', nl2br(htmlentities($ex->getTraceAsString()));
    }
} catch (Exception $ex) {
    if ($di && $di->has('logger')) {
        $di->get('logger')->error(print_r($ex, true));
    }

    echo '<pre>', $ex->getMessage(), '<br>', nl2br(htmlentities($ex->getTraceAsString()));
}
