<?php

/** @var Phalcon\Loader $loader */

use Phalcon\Loader;

$loader = new Loader();

# Namespace registration
$loader->registerNamespaces([
    'Coolblue' => $config->realpath->app
]);

$loader->register();

# Loading vendors
require_once $config->realpath->vendor . 'autoload.php';
