<?php
namespace Coolblue\Ui\Forms;

use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Form as PhalconForm;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Validation\Validator\PresenceOf;

/**
 * Class LoginForm
 *
 * @category    coolblue
 * @package     Coolblue\Ui\Forms
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 */
class LoginForm extends PhalconForm
{
    public function initialize()
    {
        # E-mail
        $email = new Text('email', [
            'class' => 'form-control form-control-lg',
            'placeholder' => 'E-mail',
            'id' => 'window-login-email',
        ]);

        $email->addValidators([
            new PresenceOf([
                'message' => 'E-mail is obligatory',
            ]),
            new Email([
                'message' => 'Entered e-mail is invalid',
            ]),
        ]);

        $this->add($email);

        # Password
        $password = new Password('password', [
            'class' => 'form-control form-control-lg',
            'placeholder' => 'Password',
            'id' => 'window-login-password',
        ]);

        $password->addValidator(new PresenceOf([
            'message' => 'Password is obligatory',
        ]));

        $password->clear();

        $this->add($password);

        # Remember me
        $remember = new Check('remember', [
            'class' => 'form-check-input',
            'checked' => true,
            'id' => 'window-login-remember',
        ]);
        $remember->setLabel('Remember me!');
        $remember->label(['class' => 'form-check-label']);

        $this->add($remember);

        # Cross-Site Request Forgery
        $checksum = new Hidden('checksum', [
            'id' => 'window-login-checksum',
        ]);

         $checksum->addValidator(new Identical([
             'value'   => $this->security->getSessionToken(),
             'message' => 'Checksum validation failed'
         ]));

         $checksum->clear();

        $this->add($checksum);

        $this->add(new Submit('submit', [
            'value' => 'Login',
            'id' => 'window-login-trigger',
            'class' => 'btn btn-warning btn-lg trigger-auth-login',
        ]));
    }
}
