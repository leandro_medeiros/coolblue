<?php

namespace Coolblue\Ui\Forms;

use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Form;

/**
 * Class BaseForm
 *
 * @category    coolblue
 * @package     Coolblue\Ui\Forms
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 */
class BaseForm extends Form
{
    public function initialize($entity = null, $options = null)
    {
        # ID
        $this->add(new Hidden('id-h'));
        $this->add(new Text('id', [
            'placeholder' => 'ID',
            'class' => 'form-control',
            'name' => 'id',
        ]));

        $active = new Select('active', [
            true => 'Yes',
            false => 'No',
            null => 'All',
        ]);
        $active->setDefault(true);
        $active->setAttributes([
            'class' => 'form-control',
            'name' => 'active',
        ]);
        $this->add($active);

        $this->add(new Submit('modal-cancel', [
            'type' => 'button',
            'value' => 'Cancel <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>',
            'class' => 'btn btn-danger',
            'data-dismiss' => 'modal',
        ]));

        $this->add(new Submit('modal-confirm', [
            'type' => 'button',
            'value' => 'Save <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>',
            'class' => 'btn btn-success',
            'data-dismiss' => 'modal',
        ]));
    }
}
