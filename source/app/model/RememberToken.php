<?php

declare(strict_types = 1);

namespace Coolblue\Model;

use Coolblue\Model\Resource\RememberToken as ResourceModel;

/**
 * Resource model for remember_token
 *
 * @category    coolblue
 * @package     Coolblue\Model
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 * @extends     Coolblue\Model\BaseModel
 * @source      "coolblue.remember_token"
 */
class RememberToken extends BaseModel
{
    /**
     * @var string
     */
    protected static $resourceClass = ResourceModel::class;

    # Put your custom code below
    #### END AUTOCODE

    /**
     * @param int    $userId
     * @param string $token
     * @param string $userAgent
     *
     * @return array
     */
    public static function save(int $userId, string $token, string $userAgent): array
    {
        $resource = new static::$resourceClass;
        $resource->setUserId($userId)
            ->setToken($token)
            ->setUserAgent($userAgent);

        try {
            if ($resource->save()) {
                return [];
            } else {
                return $resource->getMessages();
            }
        } catch (\Exception $ex) {
            return [$ex->getMessage()];
        }
    }
}

