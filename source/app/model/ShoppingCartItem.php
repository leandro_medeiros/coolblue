<?php

declare(strict_types = 1);

namespace Coolblue\Model;

use Coolblue\Model\Resource\ShoppingCartItem as ResourceModel;

/**
 * Resource model for shopping_cart_item
 *
 * @category    coolblue
 * @package     Coolblue\Model
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 * @extends     Coolblue\Model\BaseModel
 * @source      "coolblue.shopping_cart_item"
 */
class ShoppingCartItem extends BaseModel
{
    /**
     * @var string
     */
    protected static $resourceClass = ResourceModel::class;

    # Put your custom code below
    #### END AUTOCODE
}

