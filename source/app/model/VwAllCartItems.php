<?php

declare(strict_types = 1);

namespace Coolblue\Model;

use Coolblue\Model\Resource\VwAllCartItems as ResourceModel;

/**
 * Resource model for vw_all_cart_items
 *
 * @category    coolblue
 * @package     Coolblue\Model
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 * @extends     Coolblue\Model\BaseModel
 * @source      "coolblue.vw_all_cart_items"
 */
class VwAllCartItems extends BaseModel
{
    /**
     * @var string
     */
    protected static $resourceClass = ResourceModel::class;

    # Put your custom code below
    #### END AUTOCODE
}

