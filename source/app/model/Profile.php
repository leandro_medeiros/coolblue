<?php

declare(strict_types = 1);

namespace Coolblue\Model;

use Coolblue\Model\Resource\Profile as ResourceModel;

/**
 * Resource model for profile
 *
 * @category    coolblue
 * @package     Coolblue\Model
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 * @extends     Coolblue\Model\BaseModel
 * @source      "coolblue.profile"
 */
class Profile extends BaseModel
{
    /**
     * @var string
     */
    protected static $resourceClass = ResourceModel::class;

    # Put your custom code below
    #### END AUTOCODE
}

