<?php

declare(strict_types = 1);

namespace Coolblue\Model;

use Coolblue\Model\Resource\Permissions as ResourceModel;

/**
 * Resource model for permissions
 *
 * @category    coolblue
 * @package     Coolblue\Model
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 * @extends     Coolblue\Model\BaseModel
 * @source      "coolblue.permissions"
 */
class Permissions extends BaseModel
{
    /**
     * @var string
     */
    protected static $resourceClass = ResourceModel::class;

    # Put your custom code below
    #### END AUTOCODE
}

