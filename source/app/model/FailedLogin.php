<?php

declare(strict_types = 1);

namespace Coolblue\Model;

use Coolblue\Model\Resource\FailedLogin as ResourceModel;

/**
 * Resource model for failed_login
 *
 * @category    coolblue
 * @package     Coolblue\Model
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 * @extends     Coolblue\Model\BaseModel
 * @source      "coolblue.failed_login"
 */
class FailedLogin extends BaseModel
{
    /**
     * @var string
     */
    protected static $resourceClass = ResourceModel::class;

    # Put your custom code below
    #### END AUTOCODE
}

