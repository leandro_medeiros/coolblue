<?php

declare(strict_types = 1);

namespace Coolblue\Model;

use Coolblue\Model\Resource\Product as ResourceModel;

/**
 * Resource model for product
 *
 * @category    coolblue
 * @package     Coolblue\Model
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 * @extends     Coolblue\Model\BaseModel
 * @source      "coolblue.product"
 */
class Product extends BaseModel
{
    /**
     * @var string
     */
    protected static $resourceClass = ResourceModel::class;

    # Put your custom code below
    #### END AUTOCODE
}

