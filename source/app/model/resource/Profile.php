<?php

declare(strict_types = 1);

namespace Coolblue\Model\Resource;

/**
 * Resource model for profile
 *
 * @category    coolblue
 * @package     Coolblue\Model\Resource
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 * @extends     Coolblue\Model\Resource\BaseResourceModel
 * @source      "coolblue.profile"
 */
class Profile extends BaseResourceModel
{
    /**
     * @primary
     * @identity
     * @column (type="integer", nullable=false, column="id", nativeType="int")
     *
     * @var $id
     */
    protected $id;

    /**
     * @column (type="string", nullable=false, column="name", nativeType="varchar")
     *
     * @var $name
     */
    protected $name;

    /**
     * @column (type="boolean", nullable=false, column="active", nativeType="tinyint")
     *
     * @var $active
     */
    protected $active;

    /**
     * Sets a value to the field "id"
     *
     * @param integer $value
     *
     * @return \Coolblue\Model\Resource\Profile
     */
    public function setId($value)
    {
        $this->id = parent::parseIntegerValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "id" field
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets a value to the field "name"
     *
     * @param string $value
     * @return \Coolblue\Model\Resource\Profile
     */
    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }

    /**
     * Gets the current value from the "name" field
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets a value to the field "active"
     *
     * @param boolean $value
     * @return \Coolblue\Model\Resource\Profile
     */
    public function setActive($value)
    {
        $this->active = parent::parseBooleanValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "active" field
     *
     * @return boolean
     */
    public function getActive()
    {
        return parent::getBooleanValue($this->active);
    }
    # Put your custom code below
    #### END AUTOCODE

    /**
     * Defines relations between model classes (E-R)
     *
     * @return void
     */
    public function initialize()
    {
        $this->hasMany('id', __NAMESPACE__ . '\User', 'profileId', [
            'alias' => 'user',
            'foreignKey' => [
                'message' => 'Profile cannot be deleted because it\'s used on User',
            ],
        ]);

        $this->hasMany('id', __NAMESPACE__ . '\Permissions', 'profileId', [
            'alias' => 'permissions',
        ]);
    }
}
