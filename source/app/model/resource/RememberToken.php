<?php

declare(strict_types = 1);

namespace Coolblue\Model\Resource;

/**
 * Resource model for remember_token
 *
 * @category    coolblue
 * @package     Coolblue\Model\Resource
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 * @extends     Coolblue\Model\Resource\BaseResourceModel
 * @source      "coolblue.remember_token"
 */
class RememberToken extends BaseResourceModel
{
    /**
     * @primary
     * @identity
     * @column (type="integer", nullable=false, column="id", nativeType="int")
     *
     * @var $id
     */
    protected $id;

    /**
     * @column (type="integer", nullable=false, column="userId", nativeType="int")
     *
     * @var $userId
     */
    protected $userId;

    /**
     * @column (type="string", nullable=false, column="token", nativeType="char")
     *
     * @var $token
     */
    protected $token;

    /**
     * @column (type="string", nullable=false, column="userAgent", nativeType="varchar")
     *
     * @var $userAgent
     */
    protected $userAgent;

    /**
     * @column (type="string", nullable=false, column="createdAt", nativeType="datetime")
     *
     * @var $createdAt
     */
    protected $createdAt;

    /**
     * Sets a value to the field "id"
     *
     * @param integer $value
     *
     * @return \Coolblue\Model\Resource\RememberToken
     */
    public function setId($value)
    {
        $this->id = parent::parseIntegerValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "id" field
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets a value to the field "userId"
     *
     * @param integer $value
     * @return \Coolblue\Model\Resource\RememberToken
     */
    public function setUserId($value)
    {
        $this->userId = parent::parseIntegerValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "userId" field
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Sets a value to the field "token"
     *
     * @param string $value
     * @return \Coolblue\Model\Resource\RememberToken
     */
    public function setToken($value)
    {
        $this->token = $value;
        return $this;
    }

    /**
     * Gets the current value from the "token" field
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Sets a value to the field "userAgent"
     *
     * @param string $value
     * @return \Coolblue\Model\Resource\RememberToken
     */
    public function setUserAgent($value)
    {
        $this->userAgent = $value;
        return $this;
    }

    /**
     * Gets the current value from the "userAgent" field
     *
     * @return string
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * Sets a value to the field "createdAt"
     *
     * @param string $value
     * @return \Coolblue\Model\Resource\RememberToken
     */
    public function setCreatedAt($value)
    {
        $this->createdAt = parent::parseDateTimeValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "createdAt" field
     *
     * @return string
     * @throws \Exception
     */
    public function getCreatedAt()
    {
        return parent::getDateTimeValue($this->createdAt);
    }
    # Put your custom code below
    #### END AUTOCODE

    /**
     * Before create the user assign a password
     */
    public function beforeValidationOnCreate()
    {
        if (empty($this->getCreatedAt())) {
            $this->setCreatedAt(date('Y-m-d H:i:s'));
        }
    }

    /**
     * Defines relations between model classes (E-R)
     *
     * @return void
     */
    public function initialize()
    {
        $this->belongsTo('userId', __NAMESPACE__ . '\User', 'id', [
            'alias' => 'user',
        ]);
    }
}
