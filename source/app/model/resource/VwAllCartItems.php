<?php

declare(strict_types = 1);

namespace Coolblue\Model\Resource;

/**
 * Resource model for vw_all_cart_items
 *
 * @category    coolblue
 * @package     Coolblue\Model\Resource
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 * @extends     Coolblue\Model\Resource\BaseResourceModel
 * @source      "coolblue.vw_all_cart_items"
 */
class VwAllCartItems extends BaseResourceModel
{
    /**
     * @column (type="integer", nullable=false, column="cartId", nativeType="int")
     *
     * @var $cartId
     */
    protected $cartId;

    /**
     * @column (type="integer", nullable=false, column="itemId", nativeType="int")
     *
     * @var $itemId
     */
    protected $itemId;

    /**
     * @column (type="integer", nullable=false, column="userId", nativeType="int")
     *
     * @var $userId
     */
    protected $userId;

    /**
     * @column (type="string", nullable=false, column="owner", nativeType="varchar")
     *
     * @var $owner
     */
    protected $owner;

    /**
     * @column (type="string", nullable=false, column="product", nativeType="varchar")
     *
     * @var $product
     */
    protected $product;

    /**
     * @column (type="string", nullable=false, column="class", nativeType="varchar")
     *
     * @var $class
     */
    protected $class;

    /**
     * @column (type="integer", nullable=false, column="quantity", nativeType="int")
     *
     * @var $quantity
     */
    protected $quantity;

    /**
     * @column (type="boolean", nullable=false, column="active", nativeType="tinyint")
     *
     * @var $active
     */
    protected $active;

    /**
     * @column (type="string", nullable=false, column="unitPrice", nativeType="decimal")
     *
     * @var $unitPrice
     */
    protected $unitPrice;

    /**
     * @column (type="string", nullable=false, column="cartCreatedAt", nativeType="datetime")
     *
     * @var $cartCreatedAt
     */
    protected $cartCreatedAt;

    /**
     * @column (type="string", nullable=false, column="itemCreatedAt", nativeType="datetime")
     *
     * @var $itemCreatedAt
     */
    protected $itemCreatedAt;

    /**
     * @column (type="string", nullable=false, column="totalPrice", nativeType="decimal")
     *
     * @var $totalPrice
     */
    protected $totalPrice;

    /**
     * Sets a value to the field "cartId"
     *
     * @param integer $value
     *
     * @return \Coolblue\Model\Resource\VwAllCartItems
     */
    public function setCartId($value)
    {
        $this->cartId = parent::parseIntegerValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "cartId" field
     *
     * @return integer
     */
    public function getCartId()
    {
        return $this->cartId;
    }

    /**
     * Sets a value to the field "itemId"
     *
     * @param integer $value
     * @return \Coolblue\Model\Resource\VwAllCartItems
     */
    public function setItemId($value)
    {
        $this->itemId = parent::parseIntegerValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "itemId" field
     *
     * @return integer
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * Sets a value to the field "userId"
     *
     * @param integer $value
     * @return \Coolblue\Model\Resource\VwAllCartItems
     */
    public function setUserId($value)
    {
        $this->userId = parent::parseIntegerValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "userId" field
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Sets a value to the field "owner"
     *
     * @param string $value
     * @return \Coolblue\Model\Resource\VwAllCartItems
     */
    public function setOwner($value)
    {
        $this->owner = $value;
        return $this;
    }

    /**
     * Gets the current value from the "owner" field
     *
     * @return string
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Sets a value to the field "product"
     *
     * @param string $value
     * @return \Coolblue\Model\Resource\VwAllCartItems
     */
    public function setProduct($value)
    {
        $this->product = $value;
        return $this;
    }

    /**
     * Gets the current value from the "product" field
     *
     * @return string
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Sets a value to the field "class"
     *
     * @param string $value
     * @return \Coolblue\Model\Resource\VwAllCartItems
     */
    public function setClass($value)
    {
        $this->class = $value;
        return $this;
    }

    /**
     * Gets the current value from the "class" field
     *
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Sets a value to the field "quantity"
     *
     * @param integer $value
     * @return \Coolblue\Model\Resource\VwAllCartItems
     */
    public function setQuantity($value)
    {
        $this->quantity = parent::parseIntegerValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "quantity" field
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Sets a value to the field "active"
     *
     * @param boolean $value
     * @return \Coolblue\Model\Resource\VwAllCartItems
     */
    public function setActive($value)
    {
        $this->active = parent::parseBooleanValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "active" field
     *
     * @return boolean
     */
    public function getActive()
    {
        return parent::getBooleanValue($this->active);
    }

    /**
     * Sets a value to the field "unitPrice"
     *
     * @param string $value
     * @return \Coolblue\Model\Resource\VwAllCartItems
     */
    public function setUnitPrice($value)
    {
        $this->unitPrice = $value;
        return $this;
    }

    /**
     * Gets the current value from the "unitPrice" field
     *
     * @return string
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * Sets a value to the field "cartCreatedAt"
     *
     * @param string $value
     * @return \Coolblue\Model\Resource\VwAllCartItems
     */
    public function setCartCreatedAt($value)
    {
        $this->cartCreatedAt = parent::parseDateTimeValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "cartCreatedAt" field
     *
     * @return string
     * @throws \Exception
     */
    public function getCartCreatedAt()
    {
        return parent::getDateTimeValue($this->cartCreatedAt);
    }

    /**
     * Sets a value to the field "itemCreatedAt"
     *
     * @param string $value
     * @return \Coolblue\Model\Resource\VwAllCartItems
     */
    public function setItemCreatedAt($value)
    {
        $this->itemCreatedAt = parent::parseDateTimeValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "itemCreatedAt" field
     *
     * @return string
     * @throws \Exception
     */
    public function getItemCreatedAt()
    {
        return parent::getDateTimeValue($this->itemCreatedAt);
    }

    /**
     * Sets a value to the field "totalPrice"
     *
     * @param string $value
     * @return \Coolblue\Model\Resource\VwAllCartItems
     */
    public function setTotalPrice($value)
    {
        $this->totalPrice = $value;
        return $this;
    }

    /**
     * Gets the current value from the "totalPrice" field
     *
     * @return string
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }
    # Put your custom code below
    #### END AUTOCODE
}

