<?php

namespace Coolblue\Model\Resource;

use DateTime;
use Phalcon\Mvc\Model;
use Phalcon\Text;

/**
 * Class BaseResourceModel
 *
 * @category    coolblue
 * @package     Coolblue\Model\Resource
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 */
abstract class BaseResourceModel extends Model
{
    /**
     * @param string $propertyName
     *
     * @return mixed
     */
    public function __get($propertyName)
    {
        if (!property_exists($this, $propertyName)) {
            return parent::__get($propertyName);
        }

        $getterName = 'get' . Text::camelize($propertyName);
        if (method_exists($this, $getterName)) {
            return $this->$getterName();
        } else {
            trigger_error('Getter not implemented', E_USER_WARNING);
            return $this->$propertyName;
        }
    }

    /**
     * @param string $propertyName
     * @param mixed  $value
     *
     * @return $this|void
     */
    public function __set($propertyName, $value)
    {
        if (!property_exists($this, $propertyName)) {
            return parent::__set($propertyName, $value);
        }

        $setterName = 'set' . Text::camelize($propertyName);
        if (method_exists($this, $setterName)) {
            return $this->$setterName($value);
        } else {
            trigger_error('Setter not implemented', E_USER_WARNING);
            $this->$propertyName = $value;
            return $this;
        }
    }

    /**
     * @param string $value
     *
     * @return string
     */
    protected function parseDateValue(string $value)
    {
        return $value;
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    protected function getDateValue($value)
    {
        return $value;
    }

    /**
     * @param string $value
     *
     * @return string
     */
    protected function parseDateTimeValue(string $value)
    {
        return $value;
    }

    /**
     * @param $value
     *
     * @return \DateTime
     * @throws \Exception
     */
    protected function getDateTimeValue($value)
    {
        return new DateTime($value);
    }

    /**
     * @param $value
     *
     * @return int
     */
    protected function parseIntegerValue($value)
    {
        return intval($value);
    }

    /**
     * @param $value
     *
     * @return int
     */
    protected function parseBooleanValue($value)
    {
        return (in_array(trim(strtolower($value)), ['t', '1', 1, true])) ? 1 : 0;
    }

    /**
     * @param $value
     *
     * @return bool
     */
    protected function getBooleanValue($value)
    {
        return (in_array(trim(strtolower($value)), ['t', '1', 1, true]));
    }

    /**
     * @param bool $value
     *
     * @return string
     */
    protected function getFlagValue(bool $value)
    {
        return $this->getBooleanValue($value) ? '✔' : '✖';
    }

    /**
     * @return string|string[]
     */
    public function getSQL()
    {
        $sql = $this->getPhql();
        $sql = str_replace(array("[", "]"), "", $sql);
        $sql = str_replace(":,", ",", $sql);
        $sql = str_replace(":)", ")", $sql);
        return $sql;
    }
}
