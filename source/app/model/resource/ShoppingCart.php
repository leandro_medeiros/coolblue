<?php

declare(strict_types = 1);

namespace Coolblue\Model\Resource;

/**
 * Resource model for shopping_cart
 *
 * @category    coolblue
 * @package     Coolblue\Model\Resource
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 * @extends     Coolblue\Model\Resource\BaseResourceModel
 * @source      "coolblue.shopping_cart"
 */
class ShoppingCart extends BaseResourceModel
{
    /**
     * @primary
     * @identity
     * @column (type="integer", nullable=false, column="id", nativeType="int")
     *
     * @var $id
     */
    protected $id;

    /**
     * @column (type="integer", nullable=false, column="userId", nativeType="int")
     *
     * @var $userId
     */
    protected $userId;

    /**
     * @column (type="string", nullable=false, column="createdAt", nativeType="datetime")
     *
     * @var $createdAt
     */
    protected $createdAt;

    /**
     * @column (type="boolean", nullable=false, column="active", nativeType="tinyint")
     *
     * @var $active
     */
    protected $active;

    /**
     * Sets a value to the field "id"
     *
     * @param integer $value
     *
     * @return \Coolblue\Model\Resource\ShoppingCart
     */
    public function setId($value)
    {
        $this->id = parent::parseIntegerValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "id" field
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets a value to the field "userId"
     *
     * @param integer $value
     * @return \Coolblue\Model\Resource\ShoppingCart
     */
    public function setUserId($value)
    {
        $this->userId = parent::parseIntegerValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "userId" field
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Sets a value to the field "createdAt"
     *
     * @param string $value
     * @return \Coolblue\Model\Resource\ShoppingCart
     */
    public function setCreatedAt($value)
    {
        $this->createdAt = parent::parseDateTimeValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "createdAt" field
     *
     * @return string
     * @throws \Exception
     */
    public function getCreatedAt()
    {
        return parent::getDateTimeValue($this->createdAt);
    }

    /**
     * Sets a value to the field "active"
     *
     * @param boolean $value
     * @return \Coolblue\Model\Resource\ShoppingCart
     */
    public function setActive($value)
    {
        $this->active = parent::parseBooleanValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "active" field
     *
     * @return boolean
     */
    public function getActive()
    {
        return parent::getBooleanValue($this->active);
    }
    # Put your custom code below
    #### END AUTOCODE

    /**
     * Assigns creation time before saving
     */
    public function beforeValidationOnCreate()
    {
        $this->createdAt = date('Y-m-d H:i:s');
    }

    /**
     * Defines relations between model classes (E-R)
     *
     * @return void
     */
    public function initialize()
    {
        $this->belongsTo('userId', __NAMESPACE__ . '\User', 'id', array(
            'alias' => 'user',
            'reusable' => true,
        ));

        $this->hasMany('id', __NAMESPACE__ . '\ShoppingCart', 'userId', array(
            'alias' => 'shoppingCarts',
        ));
    }
}


