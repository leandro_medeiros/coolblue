<?php

declare(strict_types = 1);

namespace Coolblue\Model\Resource;

/**
 * Resource model for product_class
 *
 * @category    coolblue
 * @package     Coolblue\Model\Resource
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 * @extends     Coolblue\Model\Resource\BaseResourceModel
 * @source      "coolblue.product_class"
 */
class ProductClass extends BaseResourceModel
{
    /**
     * @primary
     * @identity
     * @column (type="integer", nullable=false, column="id", nativeType="int")
     *
     * @var $id
     */
    protected $id;

    /**
     * @column (type="string", nullable=false, column="name", nativeType="varchar")
     *
     * @var $name
     */
    protected $name;

    /**
     * Sets a value to the field "id"
     *
     * @param integer $value
     *
     * @return \Coolblue\Model\Resource\ProductClass
     */
    public function setId($value)
    {
        $this->id = parent::parseIntegerValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "id" field
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets a value to the field "name"
     *
     * @param string $value
     * @return \Coolblue\Model\Resource\ProductClass
     */
    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }

    /**
     * Gets the current value from the "name" field
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    # Put your custom code below
    #### END AUTOCODE

    /**
     * Defines relations between model classes (E-R)
     *
     * @return void
     */
    public function initialize()
    {
        $this->hasMany('id', __NAMESPACE__ . '\Product', 'classId', [
            'alias' => 'ProductClass',
            'foreignKey' => [
                'message' => 'Class cannot be deleted because there are products assigned to it',
            ],
        ]);
    }
}

