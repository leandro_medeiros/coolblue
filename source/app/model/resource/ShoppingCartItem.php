<?php

declare(strict_types = 1);

namespace Coolblue\Model\Resource;

/**
 * Resource model for shopping_cart_item
 *
 * @category    coolblue
 * @package     Coolblue\Model\Resource
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 * @extends     Coolblue\Model\Resource\BaseResourceModel
 * @source      "coolblue.shopping_cart_item"
 */
class ShoppingCartItem extends BaseResourceModel
{
    /**
     * @primary
     * @identity
     * @column (type="integer", nullable=false, column="id", nativeType="int")
     *
     * @var $id
     */
    protected $id;

    /**
     * @column (type="integer", nullable=false, column="cartId", nativeType="int")
     *
     * @var $cartId
     */
    protected $cartId;

    /**
     * @column (type="integer", nullable=false, column="productId", nativeType="int")
     *
     * @var $productId
     */
    protected $productId;

    /**
     * @column (type="integer", nullable=false, column="quantity", nativeType="int")
     *
     * @var $quantity
     */
    protected $quantity;

    /**
     * @column (type="string", nullable=false, column="createdAt", nativeType="datetime")
     *
     * @var $createdAt
     */
    protected $createdAt;

    /**
     * Sets a value to the field "id"
     *
     * @param integer $value
     *
     * @return \Coolblue\Model\Resource\ShoppingCartItem
     */
    public function setId($value)
    {
        $this->id = parent::parseIntegerValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "id" field
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets a value to the field "cartId"
     *
     * @param integer $value
     * @return \Coolblue\Model\Resource\ShoppingCartItem
     */
    public function setCartId($value)
    {
        $this->cartId = parent::parseIntegerValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "cartId" field
     *
     * @return integer
     */
    public function getCartId()
    {
        return $this->cartId;
    }

    /**
     * Sets a value to the field "productId"
     *
     * @param integer $value
     * @return \Coolblue\Model\Resource\ShoppingCartItem
     */
    public function setProductId($value)
    {
        $this->productId = parent::parseIntegerValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "productId" field
     *
     * @return integer
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Sets a value to the field "quantity"
     *
     * @param integer $value
     * @return \Coolblue\Model\Resource\ShoppingCartItem
     */
    public function setQuantity($value)
    {
        $this->quantity = parent::parseIntegerValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "quantity" field
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Sets a value to the field "createdAt"
     *
     * @param string $value
     * @return \Coolblue\Model\Resource\ShoppingCartItem
     */
    public function setCreatedAt($value)
    {
        $this->createdAt = parent::parseDateTimeValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "createdAt" field
     *
     * @return string
     * @throws \Exception
     */
    public function getCreatedAt()
    {
        return parent::getDateTimeValue($this->createdAt);
    }
    # Put your custom code below
    #### END AUTOCODE

    /**
     * Assigns creation time before saving
     */
    public function beforeValidationOnCreate()
    {
        $this->createdAt = date('Y-m-d H:i:s');
    }

    /**
     * Defines relations between model classes (E-R)
     *
     * @return void
     */
    public function initialize()
    {
        $this->belongsTo('cartId', __NAMESPACE__ . '\ShoppingCart', 'id', array(
            'alias' => 'cart',
            'reusable' => true,
        ));

        $this->belongsTo('productId', __NAMESPACE__ . '\Product', 'id', array(
            'alias' => 'product',
            'reusable' => true,
        ));
    }
}

