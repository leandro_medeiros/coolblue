<?php

declare(strict_types = 1);

namespace Coolblue\Model\Resource;

/**
 * Resource model for permissions
 *
 * @category    coolblue
 * @package     Coolblue\Model\Resource
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 * @extends     Coolblue\Model\Resource\BaseResourceModel
 * @source      "coolblue.permissions"
 */
class Permissions extends BaseResourceModel
{
    /**
     * @primary
     * @identity
     * @column (type="integer", nullable=false, column="id", nativeType="int")
     *
     * @var $id
     */
    protected $id;

    /**
     * @column (type="integer", nullable=false, column="profileId", nativeType="int")
     *
     * @var $profileId
     */
    protected $profileId;

    /**
     * @column (type="string", nullable=false, column="resource", nativeType="varchar")
     *
     * @var $resource
     */
    protected $resource;

    /**
     * @column (type="string", nullable=false, column="action", nativeType="varchar")
     *
     * @var $action
     */
    protected $action;

    /**
     * Sets a value to the field "id"
     *
     * @param integer $value
     *
     * @return \Coolblue\Model\Resource\Permissions
     */
    public function setId($value)
    {
        $this->id = parent::parseIntegerValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "id" field
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets a value to the field "profileId"
     *
     * @param integer $value
     * @return \Coolblue\Model\Resource\Permissions
     */
    public function setProfileId($value)
    {
        $this->profileId = parent::parseIntegerValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "profileId" field
     *
     * @return integer
     */
    public function getProfileId()
    {
        return $this->profileId;
    }

    /**
     * Sets a value to the field "resource"
     *
     * @param string $value
     * @return \Coolblue\Model\Resource\Permissions
     */
    public function setResource($value)
    {
        $this->resource = $value;
        return $this;
    }

    /**
     * Gets the current value from the "resource" field
     *
     * @return string
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * Sets a value to the field "action"
     *
     * @param string $value
     * @return \Coolblue\Model\Resource\Permissions
     */
    public function setAction($value)
    {
        $this->action = $value;
        return $this;
    }

    /**
     * Gets the current value from the "action" field
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }
    # Put your custom code below
    #### END AUTOCODE

    /**
     * Defines relations between model classes (E-R)
     *
     * @return void
     */
    public function initialize()
    {
        $this->belongsTo('profileId', __NAMESPACE__ . '\Profile', 'id', [
            'alias' => 'profile',
        ]);
    }
}
