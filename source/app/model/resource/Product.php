<?php

declare(strict_types = 1);

namespace Coolblue\Model\Resource;

/**
 * Resource model for product
 *
 * @category    coolblue
 * @package     Coolblue\Model\Resource
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 * @extends     Coolblue\Model\Resource\BaseResourceModel
 * @source      "coolblue.product"
 */
class Product extends BaseResourceModel
{
    /**
     * @primary
     * @identity
     * @column (type="integer", nullable=false, column="id", nativeType="int")
     *
     * @var $id
     */
    protected $id;

    /**
     * @column (type="integer", nullable=false, column="classId", nativeType="int")
     *
     * @var $classId
     */
    protected $classId;

    /**
     * @column (type="string", nullable=false, column="name", nativeType="varchar")
     *
     * @var $name
     */
    protected $name;

    /**
     * @column (type="string", nullable=false, column="unitPrice", nativeType="decimal")
     *
     * @var $unitPrice
     */
    protected $unitPrice;

    /**
     * Sets a value to the field "id"
     *
     * @param integer $value
     *
     * @return \Coolblue\Model\Resource\Product
     */
    public function setId($value)
    {
        $this->id = parent::parseIntegerValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "id" field
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets a value to the field "classId"
     *
     * @param integer $value
     * @return \Coolblue\Model\Resource\Product
     */
    public function setClassId($value)
    {
        $this->classId = parent::parseIntegerValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "classId" field
     *
     * @return integer
     */
    public function getClassId()
    {
        return $this->classId;
    }

    /**
     * Sets a value to the field "name"
     *
     * @param string $value
     * @return \Coolblue\Model\Resource\Product
     */
    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }

    /**
     * Gets the current value from the "name" field
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets a value to the field "unitPrice"
     *
     * @param string $value
     * @return \Coolblue\Model\Resource\Product
     */
    public function setUnitPrice($value)
    {
        $this->unitPrice = $value;
        return $this;
    }

    /**
     * Gets the current value from the "unitPrice" field
     *
     * @return string
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }
    # Put your custom code below
    #### END AUTOCODE

    /**
     * Defines relations between model classes (E-R)
     *
     * @return void
     */
    public function initialize()
    {
        $this->belongsTo('classId', __NAMESPACE__ . '\ProductClass', 'id', [
            'alias' => 'class',
            'reusable' => true,
        ]);
    }
}

