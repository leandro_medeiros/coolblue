<?php

declare(strict_types = 1);

namespace Coolblue\Model\Resource;

/**
 * Resource model for user
 *
 * @category    coolblue
 * @package     Coolblue\Model\Resource
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 * @extends     Coolblue\Model\Resource\BaseResourceModel
 * @source      "coolblue.user"
 */
class User extends BaseResourceModel
{
    /**
     * @primary
     * @identity
     * @column (type="integer", nullable=false, column="id", nativeType="int")
     *
     * @var $id
     */
    protected $id;

    /**
     * @column (type="string", nullable=false, column="name", nativeType="varchar")
     *
     * @var $name
     */
    protected $name;

    /**
     * @column (type="string", nullable=false, column="email", nativeType="varchar")
     *
     * @var $email
     */
    protected $email;

    /**
     * @column (type="string", nullable=false, column="password", nativeType="char")
     *
     * @var $password
     */
    protected $password;

    /**
     * @column (type="boolean", nullable=false, column="mustChangePassword", nativeType="tinyint")
     *
     * @var $mustChangePassword
     */
    protected $mustChangePassword;

    /**
     * @column (type="integer", nullable=false, column="profileId", nativeType="int")
     *
     * @var $profileId
     */
    protected $profileId;

    /**
     * @column (type="boolean", nullable=false, column="banned", nativeType="tinyint")
     *
     * @var $banned
     */
    protected $banned;

    /**
     * @column (type="boolean", nullable=false, column="suspended", nativeType="tinyint")
     *
     * @var $suspended
     */
    protected $suspended;

    /**
     * @column (type="boolean", nullable=false, column="active", nativeType="tinyint")
     *
     * @var $active
     */
    protected $active;

    /**
     * Sets a value to the field "id"
     *
     * @param integer $value
     *
     * @return \Coolblue\Model\Resource\User
     */
    public function setId($value)
    {
        $this->id = parent::parseIntegerValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "id" field
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets a value to the field "name"
     *
     * @param string $value
     * @return \Coolblue\Model\Resource\User
     */
    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }

    /**
     * Gets the current value from the "name" field
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets a value to the field "email"
     *
     * @param string $value
     * @return \Coolblue\Model\Resource\User
     */
    public function setEmail($value)
    {
        $this->email = $value;
        return $this;
    }

    /**
     * Gets the current value from the "email" field
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets a value to the field "password"
     *
     * @param string $value
     * @return \Coolblue\Model\Resource\User
     */
    public function setPassword($value)
    {
        $this->password = $value;
        return $this;
    }

    /**
     * Gets the current value from the "password" field
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Sets a value to the field "mustChangePassword"
     *
     * @param boolean $value
     * @return \Coolblue\Model\Resource\User
     */
    public function setMustChangePassword($value)
    {
        $this->mustChangePassword = parent::parseBooleanValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "mustChangePassword" field
     *
     * @return boolean
     */
    public function getMustChangePassword()
    {
        return parent::getBooleanValue($this->mustChangePassword);
    }

    /**
     * Sets a value to the field "profileId"
     *
     * @param integer $value
     * @return \Coolblue\Model\Resource\User
     */
    public function setProfileId($value)
    {
        $this->profileId = parent::parseIntegerValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "profileId" field
     *
     * @return integer
     */
    public function getProfileId()
    {
        return $this->profileId;
    }

    /**
     * Sets a value to the field "banned"
     *
     * @param boolean $value
     * @return \Coolblue\Model\Resource\User
     */
    public function setBanned($value)
    {
        $this->banned = parent::parseBooleanValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "banned" field
     *
     * @return boolean
     */
    public function getBanned()
    {
        return parent::getBooleanValue($this->banned);
    }

    /**
     * Sets a value to the field "suspended"
     *
     * @param boolean $value
     * @return \Coolblue\Model\Resource\User
     */
    public function setSuspended($value)
    {
        $this->suspended = parent::parseBooleanValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "suspended" field
     *
     * @return boolean
     */
    public function getSuspended()
    {
        return parent::getBooleanValue($this->suspended);
    }

    /**
     * Sets a value to the field "active"
     *
     * @param boolean $value
     * @return \Coolblue\Model\Resource\User
     */
    public function setActive($value)
    {
        $this->active = parent::parseBooleanValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "active" field
     *
     * @return boolean
     */
    public function getActive()
    {
        return parent::getBooleanValue($this->active);
    }
    # Put your custom code below
    #### END AUTOCODE

    /**
     * @return void
     */
    public function beforeValidationOnCreate()
    {
        if (empty($this->password)) {
            $tempPassword = preg_replace('/[^a-zA-Z0-9]/', '', base64_encode(openssl_random_pseudo_bytes(12)));
            $this->mustChangePassword = true;
            $this->password = $this->getDI()
                ->getSecurity()
                ->hash($tempPassword);
        } else {
            $this->mustChangePassword = false;
        }

        $this->active = false;
        $this->suspended = false;
        $this->banned = false;
    }

    /**
     * Defines relations between model classes (E-R)
     *
     * @return void
     */
    public function initialize()
    {
        $this->belongsTo('profileId', __NAMESPACE__ . '\Profile', 'id', array(
            'alias' => 'profile',
            'reusable' => true,
        ));

        $this->hasMany('id', __NAMESPACE__ . '\SuccessLogin', 'userId', array(
            'alias' => 'successLogins',
            'foreignKey' => array(
                'message' => 'User cannot be deleted because he/she has activity in the system',
            ),
        ));

        $this->hasMany('id', __NAMESPACE__ . '\PasswordChange', 'userId', array(
            'alias' => 'passwordChanges',
            'foreignKey' => array(
                'message' => 'User cannot be deleted because he/she has activity in the system',
            ),
        ));

        $this->hasMany('id', __NAMESPACE__ . '\ResetPassword', 'userId', array(
            'alias' => 'resetPasswords',
            'foreignKey' => array(
                'message' => 'User cannot be deleted because he/she has activity in the system',
            ),
        ));

        $this->hasMany('id', __NAMESPACE__ . '\RememberToken', 'userId', array(
            'alias' => 'rememberTokens',
            'foreignKey' => array(
                'message' => 'User cannot be deleted because he/she has activity in the system',
            ),
        ));

        $this->hasMany('id', __NAMESPACE__ . '\FailedLogin', 'userId', array(
            'alias' => 'failedLogins',
            'foreignKey' => array(
                'message' => 'User cannot be deleted because he/she has activity in the system',
            ),
        ));

        $this->hasMany('id', __NAMESPACE__ . '\ShoppingCart', 'userId', array(
            'alias' => 'shoppingCarts',
            'foreignKey' => array(
                'message' => 'User cannot be deleted because he/she has activity in the system',
            ),
        ));
    }
}
