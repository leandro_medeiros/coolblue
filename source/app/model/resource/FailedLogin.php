<?php

declare(strict_types = 1);

namespace Coolblue\Model\Resource;

/**
 * Resource model for failed_login
 *
 * @category    coolblue
 * @package     Coolblue\Model\Resource
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 * @extends     Coolblue\Model\Resource\BaseResourceModel
 * @source      "coolblue.failed_login"
 */
class FailedLogin extends BaseResourceModel
{
    /**
     * @primary
     * @identity
     * @column (type="integer", nullable=false, column="id", nativeType="int")
     *
     * @var $id
     */
    protected $id;

    /**
     * @column (type="integer", nullable=true, column="userId", nativeType="int")
     *
     * @var $userId
     */
    protected $userId;

    /**
     * @column (type="string", nullable=false, column="ipAddress", nativeType="char")
     *
     * @var $ipAddress
     */
    protected $ipAddress;

    /**
     * @column (type="string", nullable=false, column="attemptedAt", nativeType="datetime")
     *
     * @var $attemptedAt
     */
    protected $attemptedAt;

    /**
     * Sets a value to the field "id"
     *
     * @param integer $value
     *
     * @return \Coolblue\Model\Resource\FailedLogin
     */
    public function setId($value)
    {
        $this->id = parent::parseIntegerValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "id" field
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets a value to the field "userId"
     *
     * @param integer $value
     * @return \Coolblue\Model\Resource\FailedLogin
     */
    public function setUserId($value)
    {
        $this->userId = parent::parseIntegerValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "userId" field
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Sets a value to the field "ipAddress"
     *
     * @param string $value
     * @return \Coolblue\Model\Resource\FailedLogin
     */
    public function setIpAddress($value)
    {
        $this->ipAddress = $value;
        return $this;
    }

    /**
     * Gets the current value from the "ipAddress" field
     *
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * Sets a value to the field "attemptedAt"
     *
     * @param string $value
     * @return \Coolblue\Model\Resource\FailedLogin
     */
    public function setAttemptedAt($value)
    {
        $this->attemptedAt = parent::parseDateTimeValue($value);
        return $this;
    }

    /**
     * Gets the current value from the "attemptedAt" field
     *
     * @return string
     * @throws \Exception
     */
    public function getAttemptedAt()
    {
        return parent::getDateTimeValue($this->attemptedAt);
    }
    # Put your custom code below
    #### END AUTOCODE

    /**
     * Before create the user assign a password
     */
    public function beforeValidationOnCreate()
    {
        if (empty($this->getAttemptedAt())) {
            $this->setAttemptedAt(date('Y-m-d H:i:s'));
        }
    }

    /**
     * Defines relations between model classes (E-R)
     *
     * @return void
     */
    public function initialize()
    {
        $this->belongsTo('userId', __NAMESPACE__ . '\User', 'id', [
            'alias' => 'user',
        ]);
    }
}
