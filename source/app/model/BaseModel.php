<?php

namespace Coolblue\Model;

use Coolblue\Model\Resource\BaseResourceModel;
use Phalcon\DiInterface;
use Phalcon\Mvc\Model\ManagerInterface;
use Phalcon\Text;

/**
 * Class BaseModel
 *
 * @category    coolblue
 * @package     Coolblue\Model
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 */
abstract class BaseModel
{
    /**
     * @var string
     */
    protected static $resourceClass = BaseResourceModel::class;

    /**
     * @var BaseResourceModel
     */
    private $resource;

    /**
     * BaseModel constructor.
     *
     * @param null                                            $data
     * @param \Phalcon\DiInterface|null                       $dependencyInjector
     * @param \Phalcon\Mvc\Model\ManagerInterface|null        $modelsManager
     * @param \Coolblue\Model\Resource\BaseResourceModel|null $resourceModel
     */
    public function __construct(
        $data = null,
        DiInterface $dependencyInjector = null,
        ManagerInterface $modelsManager = null,
        BaseResourceModel $resourceModel = null
    ) {
        if (isset($resourceModel)) {
            $this->setResource($resourceModel);
        } else {
            $this->setResource(new static::$resourceClass(
                $data,
                $dependencyInjector,
                $modelsManager
            ));
        }
    }

    /**
     * @return BaseResourceModel
     */
    public function getResource(): BaseResourceModel
    {
        return $this->resource;
    }

    /**
     * @param \Coolblue\Model\Resource\BaseResourceModel $resource
     *
     * @return $this
     */
    protected function setResource(BaseResourceModel $resource)
    {
        $this->resource = $resource;
        return $this;
    }

    /**
     * @param $propertyName
     *
     * @return mixed
     */
    public function __get($propertyName)
    {
        /** @var string $getterName */
        $getterName = 'get' . Text::camelize($propertyName);
        return $this->getResource()->$getterName();
    }

    /**
     * @param $propertyName
     * @param $value
     *
     * @return $this
     */
    public function __set($propertyName, $value)
    {
        /** @var string $setterName */
        $setterName = 'set' . Text::camelize($propertyName);
        return $this->getResource()->$setterName($value);
    }

    /**
     * @param $method
     * @param $arguments
     *
     * @return mixed
     */
    public function __call(
        $method,
        $arguments
    ) {
        $result = call_user_func_array([$this->getResource(), $method], $arguments);

        if ($result instanceof static::$resourceClass) {
            $this->setResource($result);
            return $this;
        }

        return $result;
    }

    /**
     * @param $method
     * @param $arguments
     *
     * @return mixed
     */
    public static function __callStatic(
        $method,
        $arguments
    ) {
        $result = call_user_func_array(
            [static::$resourceClass, $method],
            $arguments
        );

        if ($result instanceof static::$resourceClass) {
            $result = new static(null, null, null, $result);
        }

        return $result;
    }

    /**
     * @param array  $searchParams
     * @param string $idFieldName
     *
     * @return array
     */
    public static function getList(array $searchParams = [], string $idFieldName = 'id')
    {
        /** @var array $result */
        $result = [];

        /** @var |Phalcon\Mvc\Model\Row $element */
        foreach (static::$resourceClass::find($searchParams) as $element) {
            $element = $element->toArray();
            $result[$element[$idFieldName]] = $element;
        }

        return $result;
    }
}
