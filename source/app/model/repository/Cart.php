<?php

declare(strict_types = 1);

namespace Coolblue\Model\Repository;

use Coolblue\Model\Resource\ShoppingCart as ShoppingCartResource;
use Coolblue\Model\Resource\User as UserResource;
use Coolblue\Model\ShoppingCart as ShoppingCartModel;
use Coolblue\Model\VwAllCartItems;
use DateTime;

/**
 * Class Cart
 *
 * @category    coolblue
 * @package     Coolblue\Model\Repository
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 */
class Cart
{
    /**
     * @param int $userId
     *
     * @return \Phalcon\Mvc\Model\ResultsetInterface
     */
    public static function getListByUser(int $userId)
    {
        return ShoppingCartModel::query()
            ->where('userId = :userId:')
            ->andWhere('active = true')
            ->orderBy('createdAt DESC')
            ->columns([
                'id',
                'DATE_FORMAT(createdAt, :date_format:) AS createdAt'
            ])
            ->bind([
                'userId' => $userId,
                'date_format' => '%M %D'
            ])
            ->execute();
    }

    /**
     * @param int $userId
     *
     * @return \Phalcon\Mvc\Model\ResultsetInterface
     */
    public static function getAllActiveList(int $userId)
    {
        return ShoppingCartModel::query()
            ->where(ShoppingCartResource::class . '.active = true')
            ->orderBy('ownedByUser DESC, createdAt DESC')
            ->columns([
                ShoppingCartResource::class . '.id',
                'userId',
                'DATE_FORMAT(createdAt, :date_format:) AS createdAt',
                '(userId = :userId:) AS ownedByUser',
                'u.name AS owner'
            ])
            ->bind([
                'userId' => $userId,
                'date_format' => '%M %D'
            ])
            ->innerJoin(UserResource::class, null, 'u')
            ->execute();
    }

    /**
     * @param int $cartId
     *
     * @return array
     */
    public static function getCartItems(int $cartId)
    {
        return VwAllCartItems::getList([
            'order' => 'itemCreatedAt DESC',
            'conditions' => 'cartId = :cartId:',
            'bind' => [
                'cartId' => $cartId
            ],
        ], 'itemId');
    }

    /**
     * @param int $cartId
     *
     * @return array
     * @throws \Exception
     */
    public static function getInfo(int $cartId)
    {
        $items = self::getCartItems($cartId);

        if (empty($items)) {
            return ['msg' => 'There are no items in this shopping cart yet.'];
        }

        $first = reset($items);
        $createdAt = new DateTime($first['cartCreatedAt']);
        $result = [
            'owner' => $first['owner'],
            'createdAt' => $createdAt->format('d M Y \a\t H:i'),
            'totalItems' => count($items),
            'totalPrice' => array_reduce($items, function(&$res, $item) {
                return $res + $item['totalPrice'];
            }, 0)
        ];

        foreach ($items as $item) {
            $createdAt = new DateTime($item['itemCreatedAt']);
            $result['items'][] = [
                $createdAt->format('d/m/y \a\t H:i'),
                $item['product'],
                $item['class'],
                $item['quantity'],
                $item['unitPrice'],
                $item['totalPrice']
            ];
        }

        return $result;
    }
}
