<?php

declare(strict_types = 1);

namespace Coolblue\Model\Repository;

use Coolblue\Library\Util\Exception as CbException;
use Coolblue\Model\RememberToken;
use Coolblue\Model\SuccessLogin;
use Coolblue\Model\User as UserModel;
use Phalcon\Http\RequestInterface;

/**
 * Class User
 *
 * @category    coolblue
 * @package     Coolblue\Model\Repository
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 */
class User
{
    /**
     * @param string $email
     *
     * @return UserModel|bool
     */
    public static function getByEmail(string $email)
    {
        $user = new UserModel;
        $user->findFirst([
            'conditions' => 'email = :email:',
            'bind'       => [
                'email' => $email,
            ]
        ]);

        return $user;
    }

    /**
     * @param int $userId
     *
     * @return UserModel|bool
     */
    public static function getById(int $userId)
    {
        $user = new UserModel;
        $user->findFirst([
            'id' => $userId
        ]);

        return $user;
    }

    /**
     * @param \Coolblue\Model\User           $user
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @throws \Coolblue\Library\Util\Exception
     */
    public function saveSuccessLogin(UserModel $user, RequestInterface $request)
    {
        $messages = SuccessLogin::save(
            (int) $user->getId(),
            $request->getClientAddress(),
            $request->getUserAgent()
        );

        if (!empty($messages)) {
            throw new CbException(
                $messages[0],
                CbException::ERR_MODEL_SAVE_FAIL
            );
        }
    }

    /**
     * @param UserModel $user
     * @param string    $userAgent
     *
     * @return bool|string
     */
    public function createRememberEnvironment(UserModel $user, string $userAgent)
    {
        /** @var string $token */
        $token = md5($user->getEmail() . $user->getPassword(). $userAgent);

        $result = RememberToken::save(
            (int) $user->getId(),
            $token,
            $userAgent
        );

        return empty($result) ? $token : false;
    }
}
