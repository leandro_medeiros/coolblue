<?php

declare(strict_types = 1);

namespace Coolblue\Model;

use Coolblue\Model\Resource\ShoppingCart as ResourceModel;

/**
 * Resource model for shopping_cart
 *
 * @category    coolblue
 * @package     Coolblue\Model
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 * @extends     Coolblue\Model\BaseModel
 * @source      "coolblue.shopping_cart"
 */
class ShoppingCart extends BaseModel
{
    /**
     * @var string
     */
    protected static $resourceClass = ResourceModel::class;

    # Put your custom code below
    #### END AUTOCODE
}

