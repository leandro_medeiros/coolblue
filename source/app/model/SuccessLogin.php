<?php

declare(strict_types = 1);

namespace Coolblue\Model;

use Coolblue\Model\Resource\SuccessLogin as ResourceModel;

/**
 * Resource model for success_login
 *
 * @category    coolblue
 * @package     Coolblue\Model
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 * @extends     Coolblue\Model\BaseModel
 * @source      "coolblue.success_login"
 */
class SuccessLogin extends BaseModel
{
    /**
     * @var string
     */
    protected static $resourceClass = ResourceModel::class;

    # Put your custom code below
    #### END AUTOCODE

    /**
     * @param int    $userId
     * @param string $ipAddress
     * @param string $userAgent
     *
     * @return array
     */
    public static function save(int $userId, string $ipAddress, string $userAgent): array
    {
        $resource = new static::$resourceClass;
        $resource->setUserId($userId)
            ->setIpAddress($ipAddress)
            ->setUserAgent($userAgent);

        if ($resource->save()) {
            return [];
        } else {
            return $resource->getMessages();
        }
    }
}

