<?php

declare(strict_types = 1);

namespace Coolblue\Model;

use Coolblue\Model\Resource\ProductClass as ResourceModel;

/**
 * Resource model for product_class
 *
 * @category    coolblue
 * @package     Coolblue\Model
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 * @extends     Coolblue\Model\BaseModel
 * @source      "coolblue.product_class"
 */
class ProductClass extends BaseModel
{
    /**
     * @var string
     */
    protected static $resourceClass = ResourceModel::class;

    # Put your custom code below
    #### END AUTOCODE
}

