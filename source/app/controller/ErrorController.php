<?php

namespace Coolblue\Controller;

/**
 * Class ErrorController
 *
 * @category    coolblue
 * @package     Coolblue\Controller
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 */
class ErrorController extends BaseController
{
    /**
     * @var string
     */
    public static $title = 'Error';

    /**
     * @var boolean
     */
    public static $isPrivate = false;

    /**
     * @var int
     */
    public static $sortOrder = -1;

    /**
     * 401 - Unauthorized Action
     */
    public function unauthorizedAction()
    {
        $this->view->title .= ' - 401: Unauthorized';
    }

    /**
     * 403 - Forbidden Action
     */
    public function forbiddenAction()
    {
        $this->view->title .= ' - 403: Forbidden';
    }

    /**
     * 404 - Not found Action
     */
    public function notfoundAction()
    {
        $this->view->title .= ' - 404: Not found';
    }
}
