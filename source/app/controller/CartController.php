<?php

namespace Coolblue\Controller;

use Coolblue\Model\Repository\Cart as CartRepository;

/**
 * Class CartController
 *
 * @category    coolblue
 * @package     Coolblue\Controller
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 */
class CartController extends BaseController
{
    /**
     * @var string
     */
    public static $title = 'Cart';

    /**
     * @var bool
     */
    public static $isPrivate = true;

    /**
     * @var int
     */
    public static $sortOrder = 1;

    /**
     * @var bool
     */
    public static $showInNavigator = true;

    /**
     * @var CartRepository
     */
    protected $repository;

    /**
     * On Construct
     */
    public function onConstruct()
    {
        $this->repository = new CartRepository();
    }

    /**
     * Default action
     */
    public function indexAction()
    {
        $this->response->redirect('cart/list');
    }

    /**
     * Listing from current user
     */
    public function listAction()
    {
        $this->view->setVar('cartList', CartRepository::getListByUser(
            $this->auth->getIdentityId()
        ));
    }

    /**
     * Listing from all users
     */
    public function listAllAction()
    {
        $this->view->setVar('cartList', CartRepository::getAllActiveList(
            $this->auth->getIdentityId()
        ));
    }

    /**
     * @param int $cartId
     *
     * @return array
     * @throws \Exception
     */
    public function getInfoAction(int $cartId)
    {
        return CartRepository::getInfo($cartId);
    }
}
