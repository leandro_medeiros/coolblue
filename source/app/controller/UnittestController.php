<?php

namespace Coolblue\Controller;

use Coolblue\Library\Util\AbstractTestCase;
use Coolblue\Library\Util\TestList;
use DirectoryIterator;

/**
 * Class UnitTestController
 *
 * @category    coolblue
 * @package     Coolblue\Controller
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 */
class UnitTestController extends BaseController
{
    /**
     * @var string
     */
    public static $title = 'Unit Tests';

    /**
     * @var boolean
     */
    public static $isPrivate = false;

    /**
     * @var int
     */
    public static $sortOrder = 2;

    /**
     * @var bool
     */
    public static $showInNavigator = true;

    /**
     * Documentation index
     */
    public function indexAction()
    {
        $info = new TestList($this->di);
        $this->view->setVar('availableTests', $info->getTestsInfo());
        $this->view->cleanTemplateBefore();
        $this->view->setTemplateBefore('fluid');
    }

    /**
     * @param $testClass
     *
     * @return mixed
     */
    public function runAction($testClass)
    {
        return ['testResults' => AbstractTestCase::execute($testClass)];
    }
}
