<?php

namespace Coolblue\Controller;

/**
 * Class DocsController
 *
 * @category    coolblue
 * @package     Coolblue\Controller
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 */
class DocsController extends BaseController
{
    /**
     * @var string
     */
    public static $title = 'Documentation';

    /**
     * @var boolean
     */
    public static $isPrivate = false;

    /**
     * @var int
     */
    public static $sortOrder = 3;

    /**
     * @var bool
     */
    public static $showInNavigator = true;

    /**
     * Documentation index
     */
    public function indexAction() {}
}
