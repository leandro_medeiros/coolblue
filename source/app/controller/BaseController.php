<?php

namespace Coolblue\Controller;

use Coolblue\Library\Util\ModuleNavigator;
use Coolblue\Ui\Forms\LoginForm;
use Phalcon\DispatcherInterface;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

/**
 * Class BaseController
 *
 * @category    coolblue
 * @package     Coolblue\Controller
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 */
abstract class BaseController extends Controller
{
    /**
     * @var string Debugger messages index
     */
    const IDX_DEBUGGING = 'idx_001';

    /**
     * @var string
     */
    public static $title = '';

    /**
     * @var int
     *
     * Exibition order in the navigator
     */
    public static $sortOrder = 0;

    /**
     * @var bool
     */
    public static $showInNavigator = false;

    /**
     * @var bool
     */
    public static $isPrivate = true;

    /**
     * @var array
     */
    public $identity;

    /*
     * Sets up global assets
     */
    public function setupAssets()
    {
        /** @var \Phalcon\Config $requireJs */
        $requireJs = $this->config->globalAssets->requireJs;

        foreach ($this->config->globalAssets->css->toArray() as $asset) {
            $this->assets->addCss($asset);
        }

        foreach ($this->config->globalAssets->js->toArray() as $asset) {
            $this->assets->addJs($asset);
        }

        $this->assets->addJs(
            $requireJs->path,
            $requireJs->local,
            $requireJs->filter,
            $requireJs->attributes->toArray()
        );
    }

    /**
     * @return bool
     */
    public function isUserLoggedIn()
    {
        return !empty($this->identity) && array_key_exists('id', $this->identity);
    }

    /**
     * Adds standard forms to the form manager
     *
     * @return void
     */
    protected function setupForms()
    {
        if (!$this->isUserLoggedIn() && !$this->forms->has('login')) {
            $this->forms->set('login', new LoginForm);
        }
    }

    /**
     * Add navigator items
     *
     * @param \Phalcon\DispatcherInterface $dispatcher
     *
     * @return void
     */
    protected function setupNavigator(DispatcherInterface $dispatcher)
    {
        $modules = ModuleNavigator::getModules(
            $this->di,
            ($this->config->application->environment == 'DEV')
        );

        if (array_key_exists($dispatcher->getControllerName(), $modules)) {
            $modules[$dispatcher->getControllerName()]['current'] = true;
        }

        $this->view->setVar('navigatorItems', $modules);
    }

    /**
     * @param Dispatcher $dispatcher
     *
     * @return bool
     */
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $this->identity = $this->auth->getIdentity();
        $controllerName = $dispatcher->getControllerName();

        if ($this->config->application->environment == 'PROD') {
            $this->view->setTemplateBefore('default');
        } else {
            $this->view->setTemplateBefore('debug');
        }

        if (!$this->isUserLoggedIn() && $this->auth->hasRememberMe()) {
            if (!$this->auth->loginWithRememberMe()) {
                $this->flashSession->notice('Your stored authentication data was invalid or has expired, please login again.');
            } else {
                $this->flashSession->success('Welcome back!');
            }
        }

        /** @var \Coolblue\Library\Acl\Acl $this->acl */
        if (!$this->acl->isPrivate($controllerName)) {
            return true;
        }

        if (!$this->isUserLoggedIn()) {
            $this->flashSession->warning('You must authenticate in order to access the requested module.');

            $dispatcher->forward(array(
                'controller' => 'error',
                'action' => 'unauthorized',
            ));
            return false;
        }

        $actionName = $dispatcher->getActionName();
        if (!$this->acl->isAllowed($this->identity['profile'], $controllerName, $actionName)) {
            $this->flash->error(
                "You don't have permissions to perform this action: " . $actionName . '@' . $controllerName
            );

            if ($this->acl->isAllowed($this->identity['profile'], $controllerName, 'index')) {
                $dispatcher->forward(array(
                    'controller' => $controllerName,
                    'action' => 'index',
                ));
            } else {
                $dispatcher->forward(array(
                    'controller' => 'user_control',
                    'action' => 'index',
                ));
            }

            return false;
        }

        return true;
    }

    public function getAllowedActions(Dispatcher $dispatcher)
    {
        if (!$this->isUserLoggedIn()) {
            return [];
        }

        return $this->acl->getAllAllowed(
            $this->auth->getIdentityProfile(),
            $dispatcher->getControllerName()
        );
    }

    /**
     * @param Dispatcher $dispatcher
     */
    public function afterExecuteRoute(Dispatcher $dispatcher)
    {
        if ($this->request->isAjax()) {
            $this->view->disable();
            $data = $dispatcher->getReturnedValue();

            if (!empty($data)) {
                $this->response->setJsonContent($data);
                $this->response->send();
            }
        } else {
            $this->setupForms();
            $this->setupAssets();
            $this->setupNavigator($dispatcher);

            $appTitle = $this->config->application->title;
            $this->view->setVar('title', empty(static::$title) ? $appTitle : $appTitle . ' - ' . static::$title);
            $this->view->setVar('allowedActions', $this->getAllowedActions($dispatcher));
            $this->view->setVar('isUserLoggedIn', $this->isUserLoggedIn());
            $this->view->setVar('currentUser', $this->identity);
        }
    }

    /**
     * @param $data
     */
    protected static function debuggingAdd($data)
    {
        $_SESSION[self::IDX_DEBUGGING][] = $data;
    }

    public static function debuggingEcho()
    {
        if (isset($_SESSION[self::IDX_DEBUGGING])) {
            echo '<pre>';
            print_r($_SESSION[self::IDX_DEBUGGING]);
            echo '</pre>';

            unset($_SESSION[self::IDX_DEBUGGING]);
        }
    }
}
