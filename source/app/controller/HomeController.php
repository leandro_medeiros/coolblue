<?php

namespace Coolblue\Controller;

use Coolblue\Library\Util\Exception as CbException;
use Coolblue\Ui\Forms\LoginForm;

/**
 * Class HomeController
 *
 * @category    coolblue
 * @package     Coolblue\Controller
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 */
class HomeController extends BaseController
{
    /**
     * @var string
     */
    public static $title = 'Home';

    /**
     * @var bool
     */
    public static $isPrivate = false;

    /**
     * @var bool
     */
    public static $showInNavigator = true;

    /**
     * @return \Phalcon\Http\ResponseInterface
     */
    public function indexAction()
    {
        return $this->response->redirect('home/welcome');
    }

    /**
     * @return void
     */
    public function welcomeAction()
    {
    }

    /**
     * @return array
     */
    public function loginAction()
    {
        /** @var LoginForm $form */
        $form = new LoginForm;

        /** @var array $msgs */
        $messages = [];

        if (!$this->request->isPost()) {
            $messages[] = 'Invalid request format';
        }

        else if (!$form->isValid($this->request->getPost())) {
            foreach ($form->getMessages() as $error) {
                $messages[] = $error->getMessage();
            }
        } else {
            try {
                $this->auth->check($this->request->getPost());
            } catch (CbException $ex) {
                $messages[] = $ex->getMessage();
            }
        }

        return [
            'msg' => $messages,
            'error' => count($messages) > 0
        ];
    }

    /**
     * @return void
     */
    public function logoutAction()
    {
        $this->identity = [];
        $this->auth->remove();
        $this->session->destroy();
        $this->response->redirect();
    }
}
