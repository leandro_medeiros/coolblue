<!-- Navigation bar -->
{% include config.view.componentsDir ~ 'navigator' %}

<!-- Main content -->
<div class="container-xl overflow-auto">
    <div class="row flex-xl-nowrap">
        <!-- Module content -->
        <main class="py-lg-3 bd-content w-100" role="main">
            <!-- Alerts -->
            <div id="main-alert-container">
                {{ flashSession.output() }}
                {{ flash.output() }}
            </div>

            {{ content() }}
        </main>
    </div>
</div>

<div class="clearfix" style="height:60px"></div>

<!-- Footer -->
<footer id="footer" class="monsterfy-footer bd-footer text-muted border-top fixed-bottom bg-noise">
    <!-- Logs -->
    <div id="footer-php-log" style="overflow-y:scroll;max-height: 500px;">
        <?php \Coolblue\Controller\BaseController::debuggingEcho(); ?>
    </div>

    <!-- Log JS -->
    <div id="footer-js-log" style="display: none">
        <script type="text/javascript">
            function footerJsClear() {
                document.getElementById('footer-js-log').style.display     = 'none';
                document.getElementById('footer-js-log-content').innerHTML = '';
            }
        </script>

        <div id="footer-js-log-content" class="w-100"></div>

        <input id="footer-js-log-btn-clear"
               type="button"
               class="btn btn-danger"
               value="Clear"
               onclick="footerJsClear()"
               />
    </div>

    <!-- Developer -->
    <div class="float-right clearfix">
        <br />
        <span><strong>L. M. Medeiros</strong> &copy;2009-{{ date('Y') }}</span>
    </div>
</footer>
