<!-- Navigation bar -->
{% include config.view.componentsDir ~ 'navigator' %}

<!-- Main content -->
<div class="container-xl overflow-auto">
    <div class="row flex-xl-nowrap">
        <!-- Module content -->
        <main class="py-lg-3 bd-content w-100" role="main">
            <!-- Alerts -->
            <div id="main-alert-container">
                {{ flashSession.output() }}
                {{ flash.output() }}
            </div>

            {{ content() }}
        </main>
    </div>
</div>

<div class="clearfix" style="height:60px"></div>

<!-- Footer -->
<footer id="footer" class="monsterfy-footer bd-footer text-muted border-top fixed-bottom bg-noise">
    <!-- Developer -->
    <div class="float-right clearfix">
        <br />
        <span><strong>L. M. Medeiros</strong> &copy;2009-{{ date('Y') }}</span>
    </div>
</footer>
