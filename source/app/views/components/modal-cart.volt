<!-- DataTables CSS -->
{{  stylesheet_link (config.publicpath.css~'vendor/jquery.dataTables.min.css') }}

<!-- Modal - Cart Info -->
<div class="modal fade" id="window-cart" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="window-cart-title" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info">
                {{ image_input(
                    'alt': 'Logo',
                    'class': 'rounded d-block',
                    'id': 'img_logo',
                    'src': config.publicpath.images~'logo/coolblue.png',
                    'height': '64'
                ) }}
                <h1 id="window-cart-title" class="modal-title w-100 text-white text-center">Shopping Cart Content</h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body" id="window-cart-body">
                <div class="clearfix"></div>
                <h3 class="panel-title py-3">Cart Items</h3>
                <input type="hidden" id="window-cart-id" value="" />

                <!-- Alert container -->
                <div id="window-cart-alert"></div>

                <!-- Table container -->
                <div class="form-group">
                    <table id="window-cart-table-items" class="datatables basic table table-hover table-borderless table-striped">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Added to Cart</th>
                            <th scope="col">Product</th>
                            <th scope="col">Type</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Unit Price</th>
                            <th scope="col">Total Price</th>
                        </tr>
                        </thead>
                    </table>
                </div>

                <!-- Extra Info -->
                <hr />

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title py-3">Summary</h4>
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <td>Owner</td>
                                <td id="window-cart-owner"></td>
                            </tr>
                            <tr>
                                <td>Created On</td>
                                <td id="window-cart-created-at"></td>
                            </tr>
                            <tr>
                                <td>Total Items</td>
                                <td id="window-cart-total-items"></td>
                            </tr>
                            <tr>
                                <td>TOTAL PRICE</td>
                                <td id="window-cart-total-price"></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer bg-info">
                <div id="window-cart-actions">
                    <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
