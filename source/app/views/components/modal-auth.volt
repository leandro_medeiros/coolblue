<input type="hidden" class="require-module" data-module-path="app/login.min" />

<!-- Modal - Auth -->
<div class="modal fade" id="window-login" tabindex="-1" role="dialog" aria-labelledby="window-login-title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <div class="w-100">
                    {{ image_input(
                        'alt': 'Logo',
                        'class': 'rounded d-block mx-auto',
                        'id': 'img_logo',
                        'src': config.publicpath.images~'logo/coolblue.png',
                        'height': '64'
                    ) }}
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body" id="window-login-forms">
                <h4 class="text-lg-center" id="window-login-msg-text">Please insert your credentials</h4>

                <hr />

                <!-- Alerts -->
                <div id="window-login-alert-container"></div>

                <!-- Login form -->
                {{ form('id': 'window-login-form') }}
                    {{ forms.get('login').render('checksum', ['value': security.getToken()]) }}

                    <p>{{ forms.get('login').render('email') }}</p>

                    <p>{{ forms.get('login').render('password') }}</p>

                    <p class="text-lg-right">
                        {{ forms.get('login').render('remember') }}
                        {{ forms.get('login').label('remember') }}
                    </p>
                {{ end_form() }}
            </div>
            <div class="modal-footer bg-info">
                {{ forms.get('login').render('submit') }}
            </div>
        </div>
    </div>
</div>
