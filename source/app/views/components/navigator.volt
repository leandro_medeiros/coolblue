<header class="navbar navbar-expand-lg navbar-dark sticky-top flex-column flex-md-row bd-navbar bg-dark">
    <!-- Logo -->
    <div class="d-flex">
        <a href="/" class="navbar-brand mr-0 mr-md-2 flex-fill" data-module="" id="brand-logo" title="Home page" aria-label="Home page">
            {{ image_input(
                'alt': 'Logo',
                'class': 'img-circle',
                'style': 'padding-top:10px',
                'width': '147',
                'height': '40',
                'src': config.publicpath.images~'logo/test.png'
            ) }}
        </a>
        <button class="navbar-toggler ml-5" type="button" data-toggle="collapse" data-target="#navigator-menu" aria-controls="navigator-menu" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
    <div class="navbar-nav-scroll navbar-collapse collapse text-center" id="navigator-menu">
        <ul class="navbar-nav bd-navbar-nav mr-auto mt-2 mt-lg-0">
            {%- for module in navigatorItems %}
            {% if module['current'] is defined %}
            <li class="nav-item active nav-indicator">{{ link_to(module['target'], module['title'], 'class': 'nav-link') }} <span class="sr-only">(current)</span></li>
            {% else %}
            <li class="nav-item">{{ link_to(module['target'], module['title'], 'class': 'nav-link') }}</li>
            {% endif %}
            {%- endfor -%}
        </ul>

        <!-- Dropdown menu -->
        <ul class="navbar-nav ml-md-auto">
            {% if not isUserLoggedIn %}
            <button class="btn btn-outline-info btn-lg navbar-right"
                    id="main-btn-login"
                    aria-expanded="false"
                    aria-haspopup="true"
                    data-target="#window-login"
                    data-toggle="modal"
                    role="button"
                    >
                Login
            </button>
            {% else %}
            <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" id="navbarDropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    {{ auth.getIdentityName() }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-lg-right p-2" aria-labelledby="navbarDropdown">
                    <p class="text-center text-break">Online since {{ auth.getIdentitySessionStart() }}</p>
                    <div class="dropdown-divider"></div>
                    {{
                    link_to(
                        '#',
                        'Logout',
                        'class': 'btn btn-danger btn-logout',
                        'id': 'btn-logout'
                    )
                    }}
                </div>
            </li>
            {% endif %}
        </ul>
    </div>
</header>
