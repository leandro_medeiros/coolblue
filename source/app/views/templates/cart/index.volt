<!-- Carts -->
<div class="jumbotron">
    <h1>Active Carts</h1>
    <p>These are the active carts on the system that you currently have access to:</p>

    <?php if (in_array('listAll', $allowedActions)): ?>
    <div class="btn-group btn-group-lg" role="group" aria-label="Lists">
        <a href="/cart/list" class="btn btn-outline-primary">Display mine</a>
        <a href="/cart/listAll" class="btn btn-outline-success">Display all</a>
    </div>
    <?php endif; ?>
</div>

<div class="d-flex flex-row flex-wrap justify-content-center">
    {% block shoppingCarts %}{% endblock %}
</div>

<!-- File name for require JS -->
<input type="hidden" class="require-module" data-module-path="app/cart.min" />

{% include config.view.componentsDir ~ 'modal-cart' %}
