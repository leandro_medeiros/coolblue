{# listall.volt #}
{% extends 'cart/index.volt' %}

{% block shoppingCarts %}
{%- for shoppingCart in cartList %}
<div class="card m-2 trigger-cart-get-info" data-id="{{ shoppingCart['id'] }}" style="width: 18rem;">
    <div class="card-img-top p-xl-4">
        <p class="card-text w-75 float-right">
            {% if shoppingCart['ownedByUser'] %}
            <span class="badge badge-pill badge-success text-wrap position-absolute mr-3 shadow-lg">this shopping cart belongs to you</span>
            {% else %}
            <span class="badge badge-pill badge-warning text-wrap position-absolute mr-3 shadow-lg">this shopping cart belongs to another customer</span>
            {% endif %}
        </p>
        {{ image_input(
            'class': 'w-100',
            'src': config.publicpath.images~'cart/shopping-cart-small.png',
            'alt': 'Shopping Cart'
        ) }}
    </div>
    <div class="card-body text-center">
        <h6 class="card-title">Shopping cart created on {{ shoppingCart['createdAt'] }} by <strong>{{ shoppingCart['owner'] }}</strong></h6>
        <p class="card-text">
            <button type="button" class="btn btn-info btn-block">More info</button>
        </p>
    </div>
</div>
{% endfor -%}
{% endblock %}
