<!-- Main Content -->
<div class="jumbotron" >
    <h1>Coolblue Test</h1>
    <p>
        Web application developed by Leandro M. Medeiros &lt;<a href="mailto:leandro.m.medeiros@me.com">leandro.m.medeiros@me.com</a>&gt; as a solution for the Coolblue Interview test.
    </p>
    <div class="list-group">
        <a href="/cart" class="list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">01. ACTIVE CARTS</h5>
                <div><span class="badge badge-pill badge-danger">Requires authentication</span></div>
            </div>
            <p class="mb-1">List of currently active shopping carts and their respective items.</p>
        </a>
        <a href="/unitTest" class="list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">02. UNIT TESTS</h5>
            </div>
            <p class="mb-1">PHPUnit testing results.</p>
        </a>
        <a href="/docs" class="list-group-item list-group-item-action">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">03. CODE DOCUMENTATION</h5>
            </div>
            <p class="mb-1">Auto-generated interactive docs.</p>
        </a>
    </div>
</div>

{% if auth.getIdentityName() is empty %}
<div class="row-fluid">
    <div class="alert alert-dismissable alert-warning">YOU MAY USE ONE OF THE FOLLOWING USERS FOR TESTS</div>
    <table class="table table-hover table-striped table-bordered">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Name</th>
                <th scope="col">E-mail</th>
                <th scope="col">Password</th>
                <th scope="col">Type</th>
            </tr>
        </thead>
        <tbody id="table-tbody-results">
            <tr>
                <th scope="row">Admin 01</th>
                <td>admin@coolblue.nl</td>
                <td>123</td>
                <td>System Administrator</td>
            </tr>
            <tr>
                <th scope="row">Tester 01</th>
                <td>tester01@coolblue.nl</td>
                <td>123</td>
                <td>Customer</td>
            </tr>
            <tr>
                <th scope="row">Tester 02</th>
                <td>tester02@coolblue.nl</td>
                <td>123</td>
                <td>Customer</td>
            </tr>
            <tr>
                <th scope="row">Tester 03</th>
                <td>tester03@coolblue.nl</td>
                <td>123</td>
                <td>Customer</td>
            </tr>
        </tbody>
    </table>
</div>
{% endif %}
