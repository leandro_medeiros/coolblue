<div class="container-flud">
    <div class="row">
        <div class="col col-lg-3">
            <div class="list-group">
            {% for id, data in availableTests %}
                <div class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">{{ data['title'] }}</h5>
                        <small>{{ data['testCount'] }} tests</small>
                    </div>
                    <p class="mb-1">{{ data['description'] }}</p>
                    <button role="button" class="btn btn-success trigger-run-test float-right" data-id="{{ id }}">Run test</button>
                </div>
            {% endfor %}
            </div>
        </div>
        <div class="col-md-auto p-5 bg-white border border-primary rounded-lg flex-fill overflow-auto" id="test-results">
        </div>
    </div>
</div>

<!-- File name for require JS -->
<input type="hidden" class="require-module" data-module-path="app/unittest.min" />
