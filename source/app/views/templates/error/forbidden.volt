{{ content() }}

<header class="jumbotron subhead" id="overview">
	<div class="hero-unit text-center">
		<h1>Nope</h1>
		<p class="lead">You don't have permissions to this page, please contact your system administrator.</p>
		{{ image_input(
			'src': config.publicpath.images~'error/403.png',
			'alt': '403 Error - Forbidden'
		) }}
	</div>
</header>
