{{ content() }}

<header class="jumbotron subhead" id="overview">
    <div class="hero-unit text-center">
        <h1>Not so fast...</h1>
        <p class="lead">This is a private page, please log in before trying to access it</p>
        {{ image_input(
            'src': config.publicpath.images~'error/401.png',
            'alt': '401 Error - Unauthorized'
        ) }}
    </div>
</header>
