{{ content() }}

<header class="jumbotron subhead" id="overview">
	<div class="hero-unit text-center">
		<h1>What a shame! 🙈</h1>
		<p class="lead">The requested page does not exists or is unavailable at the moment.</p>
		{{ image_input(
			'src': config.publicpath.images~'error/404.png',
			'alt': '404 Error - Not Found'
		) }}
	</div>
</header>
