<!DOCTYPE html>
<html lang="en-gb">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="Content-type" content="text/html;charset=utf-8" />
        <meta name="og:locale" content="pt_br" />

        <meta name="author" content="Leandro M. Medeiros - http://about.me/leandro.medeiros" />
        <meta name="framework" content="Monsterfy" />
        <meta name="title" content="Coolblue Interview" />
        <meta name="version" content="{{ config.application.version }}" />

        <title>{{ title }}</title>

        <link rel="icon" type="image/png" href="/favicon.png" />

        <!-- CSS -->
        {{ assets.outputCss() }}
    </head>

	<body>
		{{ content() }}
	</body>

    {% if not isUserLoggedIn %}
    {% include config.view.componentsDir ~ 'modal-auth' %}
    {% endif %}
    <!-- JavaScript -->
    {{ assets.outputJs() }}
</html>
