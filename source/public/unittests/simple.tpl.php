<h1><?= (string)$result->testsuite->attributes()->name; ?></h1>
<p><strong>Date:</strong> <?= date('d/m/Y H:i:s'); ?></p>
<p><strong>File:</strong> <?= (string)$result->testsuite->attributes()->file; ?></p>
<p><strong>Running time:</strong> <?= (string)$result->testsuite->attributes()->time; ?> seconds</p>
<p><strong>Tests count:</strong> <?= (string)$result->testsuite->attributes()->tests; ?> </p>
<p><strong>Success count:</strong> <?= (string)$result->testsuite->attributes()->assertions; ?> </p>
<p><strong>Failure count:</strong> <?= (string)$result->testsuite->attributes()->failures; ?> </p>
<p><strong>Total errors:</strong> <?= (string)$result->testsuite->attributes()->errors; ?> </p>

<table class="table">
    <thead>
        <tr>
            <th>Method</th>
            <th>Running time</th>
            <th>Test line</th>
            <th>Result</th>
            <th>Messages</th>
        </tr>
    </thead>
    <tbody>
        <?php

        foreach ($result->testsuite->testcase as $case) {
            $ref = new ReflectionMethod($fullyQualifiedName, $case->attributes()->name) ;
            $comment = '';

            foreach(explode("\n", $ref->getDocComment()) as $line){
                if (preg_replace('@\W+@','',$line) != '') {
                    $comment = trim(trim($line),'*/');
                    break;
                }
            }

            echo '<tr><td><strong>', (string)$case->attributes()->name, '</strong><br>', $comment,
                '</td><td>', (string)$case->attributes()->time,
                '</td><td>', (string)$case->attributes()->line, '</td>';

            if (isset($case->error)) {
                echo '<td><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAATBJREFUeNpinMornnbo88soBhKBHa/4sqxPL2YxhjMwHFi684w9AwszAwM/NwODKD8DAy8nA8PffxCVTIwMDN9+MTB8+sbA8AWIfwDZP34zRLubHFzx/78DC0jN282LGcgFYAP+f/w0wAYw/PuHIiixeR2YfuEbhMLGbcCXr1glYZrxqQEb8PDGexTBh5JmDObPT8H5J4F8XIAJRDAy/oZght9Azm8UzSAA5kPlYRjFgP9AATBmBEkgJE9K6SAZA5WHYZQwgGpiBBnGCNKoDmT/BzmN4ZS0OsgGsDWMIBWM2AIRauJ/JENgGsBiUH0weQwDGGFO+s8Adcl/uA6QZrBGKBtuIqoXGBjgfkBmIFOM6GqgBoByVd/TJwzk5EaYecDsx8BNRir++v///48AAQYA8Zpua/IG2XMAAAAASUVORK5CYII=" /><td>',nl2br((string)$case->error),'</td>';
            } else if(isset($case->failure)){
                echo '<td><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAATBJREFUeNpinMornnbo88soBhKBHa/4sqxPL2YxhjMwHFi684w9AwszAwM/NwODKD8DAy8nA8PffxCVTIwMDN9+MTB8+sbA8AWIfwDZP34zRLubHFzx/78DC0jN282LGcgFYAP+f/w0wAYw/PuHIiixeR2YfuEbhMLGbcCXr1glYZrxqQEb8PDGexTBh5JmDObPT8H5J4F8XIAJRDAy/oZght9Azm8UzSAA5kPlYRjFgP9AATBmBEkgJE9K6SAZA5WHYZQwgGpiBBnGCNKoDmT/BzmN4ZS0OsgGsDWMIBWM2AIRauJ/JENgGsBiUH0weQwDGGFO+s8Adcl/uA6QZrBGKBtuIqoXGBjgfkBmIFOM6GqgBoByVd/TJwzk5EaYecDsx8BNRir++v///48AAQYA8Zpua/IG2XMAAAAASUVORK5CYII=" /><td>',nl2br((string)$case->failure),'</td>';
            } else {
                echo '<td><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAwtJREFUeNpcU0toE1EUvW9eZmJS82tDpW3AfqyloSoUStV+wEpxIYJbRdwIbgWXIi50JYIuxKW4UhBEUBQUaxG0Wm38taKBWtv0E5vaTzKZyfzf875Ra3WGw5s33HPuu++eS7ZdluH/h1mwL1IVPeKCnSZACAfgARbIlCvlm9/OOGMbYwMbN+o7lqzvSlxt3NaU7uvsT6W2NEQ4RzoQnstPtw29GerbfKWQmblVPKWOMVNwaPUB6pOLT1lNXX/s9kD3QOeBvsFWRzJDBSNPl80lWnaKNBqNhnt29dS5rlejJZe7C4+N+0jzfIGvpx255UTiWv+enq6WtubU+NJ7yK59gpw6BTltBhb0WVjUF6BolSDdnI5bhq2YrT+2rD5zhv0SkodoZzAmdzSmG1OZuVGY1XNgewYwzhAcPMIg/0KDIP0EPQcXoat3d8vr7MggUi8FMDtpOh06lt7ZXjue/wDfSlNItoDjixcILjBYGFGhcIb7pfaO1ENEiUjppo5I+fj4UXECRQkrzVDDwgsr86DbBuRflvzg+r0x/EbyWb5+0ZpVgemVGUg0xDdTReoQAiHMFlR5kRSNMriMrxNqzxNYOveXvOtJ/NeF6yUI1SiUBmhUwr1UKdj6WqlYcTwPuPu3rRvJOx7FwfMwlUvAkRxYWylq5ndHEwKG+t6eWp0srXBxFo9A+4P4P8ZqfxgDhsLEpeB62Lswg8Ln5ZL6lmWFgKVm2Ghl3jKUaqIziQDDTG33Yj5ZrILsobCD/4X1JNubM6d4xZrnExJamWHcRO6i9aU8pGcDDdwkigTYQdh+Vxzbt4u/J5sA5BTXyxnz+9x1U1j6s2+k1cdMR2N66iuWqmpkWnArODxOIyQgUZAJQBUSE2ATZk8az+3J3AVn1lPhJgpkyZ9hQj8kcBkgMhyuPynHQ60kCTJINClRVsCiGLjaR7a6eMMtYNwdxAiiTDZOI4pgLuhA7Bfly9UkGGwg1JhmnqeBIUpFDIvMCH+YyP/jjCJiusQNphB1iLDIhMgj5n9/r/f3pwADAFnmgBYXjD2JAAAAAElFTkSuQmCC" /><td>Pass</td>';
            }

            echo '</tr>';
        }
        ?>
    </tbody>
</table>
