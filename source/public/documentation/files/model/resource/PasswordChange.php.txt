<?php

namespace Coolblue\Model\Resource;

/**
 * Class PasswordChange
 *
 * @category    coolblue
 * @package     Coolblue\Model\Resource
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>w
 */
class PasswordChange extends BaseResourceModel
{
    /**
     * @primary
     * @identity
     * @column (type="integer", nullable=false, column="id", nativeType="int")
     *
     * @var $id
     */
    protected $id;

    /**
     * @column (type="string", nullable=false, column="ipAddress", nativeType="char")
     *
     * @var $ipAddress
     */
    protected $ipAddress;

    /**
     * @column (type="string", nullable=false, column="userAgent", nativeType="varchar")
     *
     * @var $userAgent
     */
    protected $userAgent;

    /**
     * @column (type="integer", nullable=false, column="userId", nativeType="int")
     *
     * @var $userId
     */
    protected $userId;

    /**
     * @column (type="string", nullable=false, column="createdAt", nativeType="datetime")
     *
     * @var $createdAt
     */
    protected $createdAt;

    /**
     * Sets a value to the field "id"
     *
     * @param  integer     $value
     * @return self
     */
    public function setId($value)
    {
        $this->id = parent::parseIntegerValue($value);
        return $this;
    }

    /**
     * Gets the current value for the "id" field
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets a value to the field "ipAddress"
     *
     * @param  string     $value
     * @return self
     */
    public function setIpAddress($value)
    {
        $this->ipAddress = $value;
        return $this;
    }

    /**
     * Gets the current value for the "ipAddress" field
     *
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * Sets a value to the field "userAgent"
     *
     * @param  string     $value
     * @return self
     */
    public function setUserAgent($value)
    {
        $this->userAgent = $value;
        return $this;
    }

    /**
     * Gets the current value for the "userAgent" field
     *
     * @return string
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * Sets a value to the field "userId"
     *
     * @param  integer     $value
     * @return self
     */
    public function setUserId($value)
    {
        $this->userId = parent::parseIntegerValue($value);
        return $this;
    }

    /**
     * Gets the current value for the "userId" field
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Sets a value to the field "createdAt"
     *
     * @param  string     $value
     * @return self
     */
    public function setCreatedAt($value)
    {
        $this->createdAt = parent::parseDateTimeValue($value);
        return $this;
    }

    /**
     * Gets the current value for the "createdAt" field
     *
     * @return string
     * @throws \Exception
     */
    public function getCreatedAt()
    {
        return parent::getDateTimeValue($this->createdAt);
    }
    # Put your custom code below
    #### END AUTOCODE

    /**
     * Before create the user assign a password
     */
    public function beforeValidationOnCreate()
    {
        $this->createdAt = date('Y-m-d H:i:s');
    }

    /**
     * Defines relations between model classes (E-R)
     *
     * @return void
     */
    public function initialize()
    {
        $this->belongsTo('createdBy', __NAMESPACE__ . '\User', 'id', array(
            'alias' => 'creator',
        ));
    }
}

