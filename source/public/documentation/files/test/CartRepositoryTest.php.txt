<?php

declare(strict_types = 1);

namespace Coolblue\Test;

use Coolblue\Library\Util\AbstractTestCase;
use Coolblue\Model\Repository\Cart as CartRepository;
use Phalcon\Mvc\Model\ResultsetInterface;

/**
 * Class CartRepositoryTest
 *
 * @category    coolblue
 * @package     Coolblue\Test
 * @author      Leandro M. Medeiros <leandro.m.medeiros@me.com>
 */
class CartRepositoryTest extends AbstractTestCase
{
    /**
     * @var string
     */
    public static $title = 'Cart Unit Tests';

    /**
     * @var string
     */
    public static $description = 'PHPUnit test case for the Model Repository class of the Coolblue interview test.';

    /**
     * @throws \ReflectionException
     */
    public static function setUpBeforeClass(): void
    {
        self::setUpService(CartRepository::class);
    }

    /**
     * Tests Cart::getListByUser()
     */
    public function testGetListByUser()
    {
        $result = static::$subjectObject::getListByUser(1);
        $this->assertInstanceOf(ResultsetInterface::class, $result);
    }
}

