<?php

use Coolblue\Library\Acl\Acl;
use Coolblue\Library\Auth\Auth;
use Coolblue\Library\Util\Flash;
use Coolblue\Library\Util\RedisSession;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\DI\FactoryDefault;
use Phalcon\Events\EventInterface;
use Phalcon\Events\ManagerInterface;
use Phalcon\Flash\Session;
use Phalcon\Forms\Manager as FormsManager;
use Phalcon\Logger\Adapter\File as LoggerFile;
use Phalcon\Logger\Formatter\Line as LineFormatter;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\DispatcherInterface;
use Phalcon\Mvc\Model\MetaData\Redis as RedisMetaData;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;

/** @var FactoryDefault $di */
$di = new FactoryDefault();

# Setting global configuration
$di->set('config', $config);

# URL Service
$di->set('url', function () use ($config) {
    return new UrlResolver();
}, true);

# Authentication service
$di->set('auth', function () {
    return new Auth();
});

# Access Control Lists
$di->set('acl', function () use ($di) {
    return new Acl($di);
});

# Setting View Service
$di->set('view', function () use ($config) {
    $view = new View();
    $view->setViewsDir($config->view->templatesDir);
    $view->setLayoutsDir($config->view->layoutsDir);
    $view->registerEngines([
        '.volt' => function ($view, $di) use ($config) {
            $volt = new VoltEngine($view, $di);
            $voltOptions = [
                'compiledPath' => $config->realpath->cache . 'volt/',
                'compiledSeparator' => '_',
            ];

            if (ENVIRONMENT == 'DEV') {
                $voltOptions['compileAlways'] = true;
                $voltOptions['stat'] = true;
            }

            $volt->setOptions($voltOptions);

            return $volt;
        },
    ]);

    return $view;
}, true);

# Setting DB Service
$di->set('db', function () use ($config) {
    return new DbAdapter([
        'host' => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname' => $config->database->dbname,
        'charset' => $config->database->charset,
        'options' => [
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES ' . $config->database->charset
        ],
    ]);
});

# Encryption service
$di->set('crypt', function() use ($config) {
    $crypt = new Phalcon\Crypt();
    $crypt->setKey($config->application->cryptSalt);
    return $crypt;
});

# Cookies service
$di->set('cookies', function () {
    $cookies = new Phalcon\Http\Response\Cookies();
    $cookies->useEncryption(true);
    return $cookies;
});

# Setting metadata cache
$di->set('modelsMetadata', function () use ($config) {
    return new RedisMetaData((array) $config->metadata);
});

# Initializing session
$di->set('session', function () use ($config) {
    $session = RedisSession::getInstance((array) $config->session);

    if ($session->status() !== $session::SESSION_ACTIVE) {
        $session->start();
    }
    return $session;
});

# Setting Dispatcher Service
$di->set('dispatcher', function () use ($di) {
    $dispatcher = new Dispatcher();
    $dispatcher->setDefaultNamespace('Coolblue\Controller');

    /** @var ManagerInterface $eventsManager */
    $eventsManager = $di->getShared('eventsManager');
    $eventsManager->attach('dispatch:beforeException', function(
        EventInterface $event,
        DispatcherInterface $dispatcher,
        Exception $exception
    ) {
        # 404 forwarding
        if (in_array($exception->getCode(), [
            Phalcon\Dispatcher::EXCEPTION_HANDLER_NOT_FOUND,
            Phalcon\Dispatcher::EXCEPTION_ACTION_NOT_FOUND,
        ])) {
            $dispatcher->forward([
                'controller' => 'error',
                'action' => 'notfound'
            ]);
        }
    });

    $dispatcher->setEventsManager($eventsManager);

    return $dispatcher;
});

# Setting router
$di->set('router', function () use ($config) {
    return require $config->realpath->config . 'routes.php';
});

# Message services
$di->set('flash', function () {
    return new Flash([
        'error' => 'alert alert-dismissable alert-danger',
        'success' => 'alert alert-dismissable alert-success',
        'notice' => 'alert alert-dismissable alert-info',
        'warning' => 'alert alert-dismissable alert-warning',
    ]);
});

$di->set('flashSession', function () {
    return new Session([
        'error' => 'alert alert-danger',
        'success' => 'alert alert-success',
        'notice' => 'alert alert-info',
        'warning' => 'alert alert-dismissable alert-warning',
    ]);
});

# Log service
$di->set('logger', function () use ($config) {
    $formatter = new LineFormatter($config->logger->format, $config->logger->date);
    $logger = new LoggerFile($config->logger->path . DIRECTORY_SEPARATOR . $config->logger->filename);

    $logger->setFormatter($formatter);
    $logger->setLogLevel($config->logger->logLevel);

    return $logger;
});

$di->set('forms', function () {
    return new FormsManager;
});

