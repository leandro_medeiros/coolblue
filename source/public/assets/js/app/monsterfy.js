define(['jquery', 'bootbox'], function($, popup) {
    function requireModule(modulePath) {
        require([modulePath], function(module) {
            module.initialize();
        });
    }

    function callAction(actionKey, module, form)
    {
        document.getElementById('action').value = (typeof actionKey == 'undefined') ? '' : actionKey;
        document.getElementById('module').value = (typeof module == 'undefined') ? '' : module;
        document.getElementById(form).submit();
    }

    function submitForm(idForm, idCallback, module) {
        $(document.body).on('click', '#'+idCallback, function() {
            document.getElementById('module-hidden').value = module;
            $('#'+idForm).submit();
        });
    }

    function post(params, path, method) {
        path   = path   || './';
        method = method || 'POST';

        let form = document.createElement('form');
        form.setAttribute('method', method);
        form.setAttribute('action', path);

        for (const key in params) {
            if (params.hasOwnProperty(key)) {
                const hiddenField = document.createElement('input');
                hiddenField.setAttribute('type',  'hidden');
                hiddenField.setAttribute('name',  key);
                hiddenField.setAttribute('value', params[key]);

                form.appendChild(hiddenField);
            }
        }

        document.body.appendChild(form);
        form.submit();
    }

    function getScreenSize() {
        return $(window).width();
    }

    function footerLog(data) {
        let div = $('#footer-js-log');

        if (typeof div != 'undefined') {
            $('#footer-js-log-content').html('<pre>' + JSON.stringify(data) + '</pre>');

            console.log(data);

            div.show();

            return true;
        }

        return false;
    }

    function appendAlert(parentSelector, message, type, autodismiss) {
        if (typeof(type) === 'undefined') type = 'danger';
        if (typeof(autodismiss) === 'undefined') autodismiss = true;

        const button = $('<button>')
            .addClass('close')
            .attr('data-dismiss', 'alert')
            .attr('aria-label', 'Close')
            .html('<span aria-hidden="true">&times;</span>');

        const newAlert = $('<div>')
            .attr('id', 'monsterfy-alert-' + (Math.random() * 10))
            .addClass('alert alert-dismissible')
            .addClass('alert-' + type)
            .css('display', 'none')
            .append(message)
            .append(button);

        $(parentSelector).append(newAlert);

        newAlert.fadeIn(1000);

        if (autodismiss) {
            setTimeout(function () {
                newAlert.fadeOut(3000);
                // newAlert.remove();
            }, 4000);
        }
    }

    function getAjaxForm(formSelector) {
        const form = $(formSelector);
        let values = {};

        if (typeof form == 'undefined') {
            return;
        }

        $.each($(form).serializeArray(), function(i, field) {
            values[field.name] = field.value;
        });

        return {
            type     : $(form).attr('method'),
            url      : $(form).attr('action'),
            dataType : 'json',
            data     : values
        };
    }

    function isInteger(value) {
        return (typeof value === 'number') && (value == parseInt(value, 10));
    }

    function isNumber(value) {
        return (typeof value === 'number') && (value == parseFloat(value, 10));
    }

    function initialize() {
        $('.require-module').each(function (idx, obj) {
            requireModule($(obj).attr('data-module-path'));
        });

        $(".modal-wide").on("show.bs.modal", function() {
            const height = $(window).height() - 100;
            $(this).find(".modal-body").css("max-height", height);
        });

        $('.form-control').keypress(function(event) {
            if (event.which == 13) {
                const target = $($(this).attr('data-target'));
                if (target) {
                    event.preventDefault();
                    target.click();
                }
            }
        });

        $('.call-action').click(function() {
            const action = $(this).attr('data-action');
            const module = $(this).attr('data-module');
            const form   = $(this).attr('data-form');
            callAction(action, module, form);
        });

        $('.btn-logout').click(function(event) {
            event.preventDefault();
            popup.confirm('Are you sure you want to logout?', function(result) {
                if (result === true) location.replace('/home/logout');
            });
        });
    }

    return {
        initialize    : initialize,
        callAction    : callAction,
        getScreenSize : getScreenSize,
        submitForm    : submitForm,
        footerLog     : footerLog,
        appendAlert   : appendAlert,
        getAjaxForm   : getAjaxForm,
        post          : post,
        isInteger     : isInteger,
        isNumber      : isNumber
    };
});
