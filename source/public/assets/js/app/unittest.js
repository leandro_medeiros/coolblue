define([
    'jquery',
    'monsterfy'
], function($, app) {
    function initialize() {
        $('body').on('click', '.trigger-run-test', function (event) {
            event.preventDefault();
            runTest($(this));
        });
    }

    function runTest(element) {
        const testId = element.attr('data-id');
        const resultContainer = $('#test-results');
        resultContainer.html('Running test...');

        if (!testId) {
            return;
        }

        $.ajax({
            type     : 'POST',
            url      : '/unittest/run/' + testId + '/' + Math.random(),
            dataType : 'json',
        }).done(function(event) {
            if (!event.testResults) {
                return app.appendAlert(
                    '#main-alert-container',
                    'Unit test run failed',
                    'danger',
                    false
                );
            }

            resultContainer.html(event.testResults);
        }).fail(function(ex) {
            const msg = ex.responseText || 'Request failed.';
            app.appendAlert('#main-alert-container', msg);
            app.footerLog(ex);
            resultContainer.html('');
        });
    }

    return {
        initialize : initialize
    };
});
