define([
    'jquery',
    'bootstrap',
    'monsterfy',
    'monsterfyTable'
], function($, bootstrap, app, appTable) {
    function initialize() {
        appTable.initialize('#window-cart-table-items', [
            {'className': 'dt-header-center'},
            {'className': 'dt-header-center'},
            {'className': 'dt-header-center'},
            {'className': 'dt-center'},
            {'className': 'dt-center'},
            {'className': 'dt-center'}
        ]);

        $('#window-cart').on('show.bs.modal', function (event) {
            cartGetData();
        });

        $('#window-cart').on('hide.bs.modal', function (event) {
            appTable.clear('#window-cart-table-items');
            $('#window-cart-alert').html('');
            $('#window-cart-owner').html('');
            $('#window-cart-created-at').html('');
            $('#window-cart-total-items').html('');
            $('#window-cart-total-price').html('');
        });

        $('body').on('click', '.trigger-cart-get-info', function (event) {
            event.preventDefault();
            cartShowModal($(this));

            setTimeout(function () {
                appTable.adjustColumnSizing('#window-cart-table-items');
            },450);
        });
    }

    function cartShowModal(element) {
        $('#window-cart-id').val(element.attr('data-id'));
        $('#window-cart').modal('show');
    }

    function cartGetData(cartId) {
        cartId = cartId || $('#window-cart-id').val();

        if (!cartId) {
            $('#window-cart').modal('hide');
            return;
        }

        $.ajax({
            type     : 'POST',
            url      : '/cart/getInfo/' + cartId,
            dataType : 'json'
        }).done(function(cart) {
            if (!cart.items) {
                return app.appendAlert(
                    '#window-cart-alert',
                    'There are no items in this shopping cart yet.',
                    'danger',
                    false
                );
            }

            appTable.setData('#window-cart-table-items', cart.items);
            $('#window-cart-owner').html(cart.owner);
            $('#window-cart-created-at').html(cart.createdAt);
            $('#window-cart-total-items').html(cart.totalItems);
            $('#window-cart-total-price').html(cart.totalPrice);
        }).fail(function(ex) {
            $('#window-cart').modal('hide');

            const msg = ex.responseText || 'Request failed.';
            app.appendAlert('#main-alert-container', msg);
            app.footerLog(ex);
        });
    }

    return {
        initialize : initialize
    };
});
