define(['jquery', 'monsterfy'], function($, app) {
    const MSG_ANIMATE_TIME = 150;
    const MSG_SHOW_TIME    = 3000;

    function initialize() {
        const formLogin = $('#window-login-form');

        $('#window-login').on('click', '.trigger-auth-login', function(event) {
            event.preventDefault();
            login();
        });
    }

    /**
     * @param oldForm
     * @param newForm
     */
    function modalAnimate(oldForm, newForm) {
        const oldH = oldForm.height();
        const newH = newForm.height();

        $('#window-login').css('height', oldH);

        oldForm.fadeToggle(MSG_ANIMATE_TIME, function() {
            $('#window-login').animate({height: newH}, MSG_ANIMATE_TIME, function() {
                newForm.fadeToggle(300);
            });
        });
    }

    function login() {
        const trigger = $('#window-login-trigger');
        trigger.attr('value', 'Checking credentials...').prop('disabled', true);

        $.ajax({
            type     : 'POST',
            dataType : 'json',
            url      : '/home/login/',
            data     : {
                email    : $('#window-login-email').val(),
                password : $('#window-login-password').val(),
                remember : $('#window-login-remember').val(),
                checksum : $('#window-login-checksum').val()
            }
        }).done(function(result) {
            if (!result) {
                app.appendAlert('#window-login-alert-container', 'Process failed');
                trigger.attr('value', 'Login').prop('disabled', false);
            } else if (!result.error) {
                app.appendAlert('#window-login-alert-container', 'Successfully authenticated', 'success');
                trigger.attr('value', 'Loading');

                setTimeout(function() {
                    location.reload();
                }, MSG_SHOW_TIME + MSG_ANIMATE_TIME);
            } else {
                app.appendAlert('#window-login-alert-container', result.msg, 'warning');
                trigger.attr('value', 'Login').prop('disabled', false);
            }
        }).fail(function(xhr) {
            trigger.attr('value', 'Login').prop('disabled', false);
            app.appendAlert('#window-login-alert-container', xhr.responseText || 'Server communication failed');
        });
    }

    return {
        initialize : initialize
    };
});
