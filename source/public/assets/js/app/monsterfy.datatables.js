define(['jquery', 'monsterfy', 'dataTables'], function($, App, dataTables) {

    function renderDefault(table, columns) {
        return $(table).dataTable({
            bAutoWidth      : false,
            deferRender     : true,
            bPaginate       : true,
            pagingType      : "full_numbers",
            bProcessing     : true,
            bStateSave      : true,
            bRetrieve       : true,
            sScrollY        : "500px",
            sScrollX        : "100%",
            bScrollCollapse : true,
            aoColumns       : columns,
            dom             : 'T<"clear">lfrtip',
            buttons         : [
                'copy', 'excel', 'pdf'
            ]
        });
    }

    function renderBasic(table, columns) {
        return $(table).dataTable({
            bAutoWidth      : false,
            bFilter         : false,
            bInfo           : false,
            deferRender     : false,
            bPaginate       : false,
            bProcessing     : false,
            bStateSave      : false,
            bRetrieve       : false,
            sScrollY        : "300px",
            sScrollX        : "100%",
            bScrollCollapse : true,
            aoColumns       : columns
        });
    }

    function initialize(tableId, columns) {
        handleDatatable();
        table = $(tableId);

        if (table.hasClass('datatables')) {
            if (table.hasClass('basic')) {
                renderBasic(table, columns, []);
            } else {
                renderDefault(table, columns, []);
            }

            table.addClass('table')
                 .addClass('table-striped')
                 .addClass('table-hover')
                 .addClass('table-row-border')
                 .addClass('table-nowrap')
                 ;

            if ($(table).attr('form-filters') !== undefined) {
                refresh(table);
            }
        }
    }

    function refresh(table, ajaxForm) {
        var datatable = $(table).dataTable();
        var filters = ajaxForm || App.getAjaxForm(
            $(table).attr('form-filters')
        );

        if (typeof filters == 'undefined' || filters == '') {
            return;
        }

        datatable.fnClearTable();

        $.ajax(
            filters
        ).done(function(result) {
            if (result) {
                datatable.fnAddData(result);
            }
        }).fail(function(ex) {
            if (ex.status != 200) {
                var msg = ex.responseText || JSON.stringify(ex);
                App.appendAlert('#main-alert-container', msg);
            }
        });

        datatable.fnDraw();
    }

    function setData(table, recordset)
    {
        datatable = $(table).dataTable();
        datatable.fnClearTable();
        if (Array.isArray(recordset) && recordset.length > 0) {
            datatable.fnAddData(recordset);
        }
        datatable.fnDraw();
    }

    function changeSearchBox(instance, elementSearch) {
        $(elementSearch).keyup(function() {
            $(instance).dataTable().fnFilter(this.value);
        });
    }

    function disableSorting(instance) {
        return $(instance).dataTable({
            "oLanguage": {
                "sUrl": DIR_DATATABLES_PT
            },
            "bPaginate": false,
            "bProcessing": true,
            "bStateSave": true,
            "bRetrieve": true,
            "sScrollX": "100%",
            "bScrollCollapse": true,
            "bAutoWidth": false,
            "bSort": false,
            "aoColumns": [
                { "bSortable": false },
                { "bSortable": false },
                { "bSortable": false }
            ]}
        );
    }

    function handleDatatable() {
        // Extensões do plugin datatables para implementação de detecção de tipos de dado.
        $.fn.dataTableExt.oSort['uk_date-asc']  = function(a,b) {
            var ukDatea = a.split('/');
            var ukDateb = b.split('/');

            var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
            var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

            return ((x < y) ? -1 : ((x > y) ?  1 : 0));
        };

        $.fn.dataTableExt.oSort['uk_date-desc'] = function(a,b) {
            var ukDatea = a.split('/');
            var ukDateb = b.split('/');

            var x = (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
            var y = (ukDateb[2] + ukDateb[1] + ukDateb[0]) * 1;

            return ((x < y) ? 1 : ((x > y) ?  -1 : 0));
        };

        /* Note 'unshift' does not work in IE6. A simply array concatenation would. This is used
         * to give the custom type top priority
         */
        $.fn.dataTableExt.aTypes.unshift(
            function ( sData )
            {
                var sValidChars = "0123456789-,";
                var Char;
                var bDecimal = false;

                /* Check the numeric part */
                for ( i=0 ; i<sData.length ; i++ )
                {
                    Char = sData.charAt(i);
                    if (sValidChars.indexOf(Char) == -1)
                    {
                        return null;
                    }

                    /* Only allowed one decimal place... */
                    if ( Char == "," )
                    {
                        if ( bDecimal )
                        {
                            return null;
                        }
                        bDecimal = true;
                    }
                }

                return 'numeric-comma';
            }
        );

        $.fn.dataTableExt.oSort['numeric-comma-asc']  = function(a,b) {
            var x = (a == "-") ? 0 : a.replace( /,/, "." );
            var y = (b == "-") ? 0 : b.replace( /,/, "." );
            x = parseFloat( x );
            y = parseFloat( y );
            return ((x < y) ? -1 : ((x > y) ?  1 : 0));
        };

        $.fn.dataTableExt.oSort['numeric-comma-desc'] = function(a,b) {
            var x = (a == "-") ? 0 : a.replace( /,/, "." );
            var y = (b == "-") ? 0 : b.replace( /,/, "." );
            x = parseFloat( x );
            y = parseFloat( y );
            return ((x < y) ?  1 : ((x > y) ? -1 : 0));
        };
    }

    function destroy(tableSelector)
    {
        $(tableSelector).dataTable().fnDestroy();
    }

    function clear(tableSelector)
    {
        $(tableSelector).dataTable().fnClearTable();
    }

    function adjustColumnSizing(tableSelector)
    {
        $(tableSelector).dataTable().fnAdjustColumnSizing();
    }

    function isDataTable(nTable) {
        var settings = $.fn.dataTableSettings;
        for ( var i=0, iLen=settings.length ; i<iLen ; i++ )
        {
            if ( settings[i].nTable == nTable )
            {
                return true;
            }
        }
        return false;
    }

    return {
        initialize         : initialize,
        refresh            : refresh,
        setData            : setData,
        destroy            : destroy,
        clear              : clear,
        adjustColumnSizing : adjustColumnSizing,
        renderDefault      : renderDefault,
        isDataTable        : isDataTable,
        changeSearchBox    : changeSearchBox,
        disableSorting     : disableSorting
    };
});
