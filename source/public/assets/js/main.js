requirejs.config({
    urlArgs : 'v=1.0.' + Math.random(),
    baseUrl : '/assets/js/',
    waitSeconds : 30,
    paths : {
        jquery : 'vendor/jquery.min',
        bootstrap : 'vendor/bootstrap.bundle.min',
        dataTables : 'vendor/jquery.dataTables.min',
        monsterfy : 'app/monsterfy.min',
        monsterfyTable : 'app/monsterfy.datatables.min',
        bootbox : 'vendor/bootbox.min'
    },
    shim : {
        'bootbox' : {
            deps    : ['jquery'],
            exports : 'bootbox'
        }, 'dataTables' : {
            deps    : ['jquery'],
            exports : 'dataTables'
        }
    }
});

requirejs([
    'jquery',
    'bootstrap',
    'monsterfy'
], function(
    $,
    bootstrap,
    app
) {
    const jQuery = $;
    const bt = bootstrap;
    app.initialize();
});
