<?php

try {
    require_once(realpath('..') . '/app/etc/bootstrap.php');
} catch (\Exception $ex) {
    echo $ex->getMessage();
} catch (\Error $err) {
    echo '<pre>', $err->getMessage(), "\n", $err->getTraceAsString();
}
